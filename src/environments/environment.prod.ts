/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  API_IP : 'http://192.168.1.24:',
  API_PORT : 9090,
};
