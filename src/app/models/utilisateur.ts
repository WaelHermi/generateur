import { Role } from './role';
import { GeneratedProject } from './generated-project';
import { TraceDonnees } from '../administration/models/trace-donnees';


export class Utilisateur {
    idUtilisateur: String;
    adresseMailUtilisateur: String;
    adresseUtilisateur: String;
    telephoneUtilisateur: String;
    passwordUtilisateur: String;
    usernameUtilisateur: String;
    prenomUtilisateur: String;
    nomUtilisateur: String;
    active: Boolean;
    dateDeNaissanceUtilisateur: Date;
    roles: Role[];
    traceDonneess: TraceDonnees[];
    generatedProjects: GeneratedProject[];
}
