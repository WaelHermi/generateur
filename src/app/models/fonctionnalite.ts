export class Fonctionnalite {
    idFonctionnalite: String;
    iconFonctionnalite: String;
    urlFonctionnalite: String;
    designationFonctionnalite: String;
    codeFonctionnalite: String;
}
