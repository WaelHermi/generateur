import { Utilisateur } from "./utilisateur";

export class GeneratedProject {

    idGeneratedProject: String;
    typeGeneratedProject: String;
    urlGeneratedProject: String;
    nomGeneratedProject: String;
    date: Date;
    utilisateur: Utilisateur;
}
