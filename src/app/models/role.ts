import { Fonctionnalite } from './fonctionnalite';

export class Role {
    idRole: String;
    nomRole: String;
    fonctionnalites: Fonctionnalite[];
}
