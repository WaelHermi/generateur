import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GestionComponent} from './gestion.component';
import {ProprieteComponent} from './propriete/propriete.component';
import {DependanceComponent} from './dependance/dependance.component';
import {DropdownModule} from 'primeng/dropdown';

import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbWindowModule,
} from "@nebular/theme";

import {ThemeModule} from "../@theme/theme.module";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DialogModule} from "primeng/dialog";
import {JwPaginationModule} from "../administration/shared/jw-pagination/jw-pagination.module";
import {GestionRoutingModule} from "./gestion-routing.module";
import {CurdComponent} from "./shared/curd/curd.component";
import {AgGridModule} from "ag-grid-angular";
import {ButtonRendererComponent} from "./button-renderer/button-renderer.component";
import {TableModule} from 'primeng/table';
import {RxReactiveFormsModule} from "@rxweb/reactive-form-validators";


@NgModule({
  declarations: [GestionComponent, ProprieteComponent, DependanceComponent, CurdComponent, ButtonRendererComponent],
  imports: [
    CommonModule,
    NbTooltipModule,
    NbSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    DialogModule,
    NbSelectModule,
    NbIconModule,
    NbInputModule,
    NbMenuModule,
    NbWindowModule,
    NbAccordionModule,
    NbRouteTabsetModule,
    NbTabsetModule,
    NbActionsModule,
    ThemeModule,
    JwPaginationModule,
    RouterModule,
    GestionRoutingModule,
    RxReactiveFormsModule,
    AgGridModule.withComponents([
      ButtonRendererComponent,
    ]),
    TableModule,
    DropdownModule
  ]
})
export class GestionModule { }
