import { Component, OnInit } from '@angular/core';
import {ProprieteApplication} from "../models/propriete-application";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataModel} from "../../shared/data-model";
import {ProprieteBase} from "../models/propriete-base";
import {ProprieteAuthentification} from "../models/propriete-authentification";
import {EnumWrapper} from "../../generation/models/enum-wrapper";
import {ProprieteApplicationService} from "../services/propriete-application.service";
import {ProprieteBaseService} from "../services/propriete-base.service";
import {ProprieteAuthentificationService} from "../services/propriete-authentification.service";
import {TypeService} from "../../generation/services/type.service";
import {SelectModel} from "../../shared/select-model";
import {RxFormBuilder, RxwebValidators} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-propriete',
  templateUrl: './propriete.component.html',
  styleUrls: ['./propriete.component.scss']
})
export class ProprieteComponent implements OnInit {

  proprietesApplicationTable: ProprieteApplication [];
  proprieteApplication: ProprieteApplication;
  proprieteApplicationForm: FormGroup;
  proprietesApplicationModel: DataModel[];

  proprietesBaseTable: ProprieteBase [];
  proprieteBase: ProprieteBase;
  proprieteBaseForm: FormGroup;
  proprietesBaseModel: DataModel [];

  proprietesAuthentificationTable: ProprieteAuthentification [];
  proprieteAuthentification: ProprieteAuthentification;
  proprieteAuthentificationForm: FormGroup;
  proprietesAuthentificationModel: DataModel [];

  scopes: EnumWrapper[];
  applicationTypes: EnumWrapper[];
  baseTypes: EnumWrapper[];
  authentificationTypes: EnumWrapper[];

  constructor(private proprieteApplicationService: ProprieteApplicationService, private proprieteBaseService: ProprieteBaseService, private proprieteAuthentificationService: ProprieteAuthentificationService, private typeService: TypeService,  private fb: RxFormBuilder) { }

  ngOnInit() {

    this.proprieteApplicationService.getAll(1000, 0).subscribe( response => this.proprietesApplicationTable = response );
    this.proprieteBaseService.getAll(1000, 0).subscribe( response => this.proprietesBaseTable = response );
    this.proprieteAuthentificationService.getAll(1000, 0).subscribe( response => this.proprietesAuthentificationTable = response );

    this.typeService.getAllApplicationTypes().subscribe( response => {
      const applicationTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
      this.proprietesApplicationModel = [
        new DataModel('idPropriete', 'ID', 'number', false, false, [], false),
        new DataModel('nomPropriete','Nom','text', true, true,  [], true),
        new DataModel('valeurPropriete','Valeur','text', true,  true,[], true),
        new DataModel('applicationType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", applicationTypesList)),
      ]
    })



    this.typeService.getAllBaseTypes().subscribe( response =>  {
      const baseTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
      this.proprietesBaseModel = [
        new DataModel('idPropriete','ID','number', false, false, [], false),
        new DataModel('nomPropriete','Nom','text', true, true,  [], true),
        new DataModel('valeurPropriete','Valeur','text', true,  true,[], true),
        new DataModel('baseType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", baseTypesList)),
      ]
    })

    this.typeService.getAllAuthentificationTypes().subscribe( response => {
      const authentificationTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
      this.proprietesAuthentificationModel = [
        new DataModel('idPropriete','ID','number', false, false, [], false),
        new DataModel('nomPropriete','Nom','text', true, true,  [], true),
        new DataModel('valeurPropriete','Valeur','text', true,  true,[], true),
        new DataModel('authentificationType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", authentificationTypesList)),
      ]
    })


    this.proprieteApplicationForm = this.fb.group({
      nomPropriete: ['', [RxwebValidators.required()]],
      valeurPropriete: ['', [RxwebValidators.required()]],
      applicationType: ['', [RxwebValidators.required()]],
    });


    this.proprieteBaseForm = this.fb.group({
      nomPropriete: ['', [RxwebValidators.required()]],
      valeurPropriete: ['', [RxwebValidators.required()]],
      baseType: ['', [RxwebValidators.required()]],
    });

    this.proprieteAuthentificationForm = this.fb.group({
      nomPropriete: ['', [RxwebValidators.required()]],
      valeurPropriete: ['', [RxwebValidators.required()]],
      authentificationType: ['', [RxwebValidators.required()]],
    });
  }

}
