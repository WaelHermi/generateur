import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'ngx-button-renderer',
  templateUrl: './button-renderer.component.html',
  styleUrls: ['./button-renderer.component.scss']
})
export class ButtonRendererComponent {

  @Input()
  update: Boolean = true;

  @Input()
  delete: Boolean = true;

  @Input()
  rowDataInput: any;

  @Output()
  updateEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  deleteEvent: EventEmitter<any> = new EventEmitter<any>();


  constructor() { }

  accesrole:boolean=false
  
  onUpdateClick() {
    this.updateEvent.emit(this.rowDataInput);
  }
  

  onDeleteClick() {
    this.deleteEvent.emit(this.rowDataInput)
  }

}
