import { Component, OnInit } from '@angular/core';
import {DependanceApplicationService} from "../services/dependance-application.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataModel} from "../../shared/data-model";
import {DependanceApplication} from "../models/dependance-application";
import {DependanceBase} from "../models/dependance-base";
import {DependanceBaseService} from "../services/dependance-base.service";
import {DependanceAuthentificationService} from "../services/dependance-authentification.service";
import {DependanceAuthentification} from "../models/dependance-authentification";
import {SelectModel} from "../../shared/select-model";
import {TypeService} from "../../generation/services/type.service";
import {EnumWrapper} from "../../generation/models/enum-wrapper";
import {RxFormBuilder, RxwebValidators} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-dependance',
  templateUrl: './dependance.component.html',
  styleUrls: ['./dependance.component.scss']
})
export class DependanceComponent implements OnInit {

  dependancesApplicationTable: DependanceApplication [];
  dependanceApplication: DependanceApplication;
  dependanceApplicationForm: FormGroup;
  dependancesApplicationModel: DataModel[];

  dependancesBaseTable: DependanceBase [];
  dependanceBase: DependanceBase;
  dependanceBaseForm: FormGroup;
  dependancesBaseModel: DataModel [];

  dependancesAuthentificationTable: DependanceAuthentification [];
  dependanceAuthentification: DependanceAuthentification;
  dependanceAuthentificationForm: FormGroup;
  dependancesAuthentificationModel: DataModel [];

  scopes: EnumWrapper[];
  applicationTypes: EnumWrapper[];
  baseTypes: EnumWrapper[];
  authentificationTypes: EnumWrapper[];

  constructor(private dependanceApplicationService: DependanceApplicationService, private dependanceBaseService: DependanceBaseService, private dependanceAuthentificationService: DependanceAuthentificationService, private typeService: TypeService,  private fb: RxFormBuilder) { }

  ngOnInit() {

    this.dependanceApplicationService.getAll(1000, 0).subscribe( response => this.dependancesApplicationTable = response );
    this.dependanceBaseService.getAll(1000, 0).subscribe( response => this.dependancesBaseTable = response );
    this.dependanceAuthentificationService.getAll(1000, 0).subscribe( response => this.dependancesAuthentificationTable = response );

    this.typeService.getAllDependanceScopes().subscribe( scopeResponse => {
      this.scopes = scopeResponse;
      const scopeList = this.scopes.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));

      this.typeService.getAllApplicationTypes().subscribe( response => {
        const applicationTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
        this.dependancesApplicationModel = [
          new DataModel('idDependance', 'ID', 'number', false, false, [], false),
          new DataModel('groupDependance', 'Groupe', 'text', true, true, [], true),
          new DataModel('artifactDependance', 'Artifact', 'text', true, true, [], true),
          new DataModel('versionDependance', 'Version', 'text', true, true, [], false),
          new DataModel('applicationType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", applicationTypesList)),
          new DataModel('dependanceScope', 'Scope', 'select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", scopeList))
        ]
      })

      this.typeService.getAllBaseTypes().subscribe( response =>  {
        const baseTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
        this.dependancesBaseModel = [
          new DataModel('idDependance','ID','number', false, false, [], false),
          new DataModel('groupDependance','Groupe','text', true, true,  [], true),
          new DataModel('artifactDependance','Artifact','text', true,  true,[], true),
          new DataModel('versionDependance','Version','text', true, true, [], false),
          new DataModel('baseType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", baseTypesList)),
          new DataModel('dependanceScope','Scope','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", scopeList)),
        ]
      })

      this.typeService.getAllAuthentificationTypes().subscribe( response => {
        const authentificationTypesList = response.map( element => ({ label: element.valeurEnum, value: element.nomEnum }));
        this.dependancesAuthentificationModel = [
          new DataModel('idDependance','ID','number', false, false, [], false),
          new DataModel('groupDependance','Groupe','text', true, true,  [], true),
          new DataModel('artifactDependance','Artifact','text', true,  true,[], true),
          new DataModel('versionDependance','Version','text', true, true, [], false),
          new DataModel('authentificationType','Type','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", authentificationTypesList)),
          new DataModel('dependanceScope','Scope','select', true, true, [], true, new SelectModel("nomEnum", "valeurEnum", scopeList)),
        ]
      })
    });


    this.dependanceApplicationForm = this.fb.group({
      groupDependance: ['', [RxwebValidators.required()]],
      artifactDependance: ['', [RxwebValidators.required()]],
      versionDependance: [''],
      applicationType: ['', [RxwebValidators.required()]],
      dependanceScope: ['', [RxwebValidators.required()]]
    });


    this.dependanceBaseForm = this.fb.group({
      groupDependance: ['', [RxwebValidators.required()]],
      artifactDependance: ['', [RxwebValidators.required()]],
      versionDependance: [''],
      baseType: ['', [RxwebValidators.required()]],
      dependanceScope: ['', [RxwebValidators.required()]]
    });

    this.dependanceAuthentificationForm = this.fb.group({
      groupDependance: ['', [RxwebValidators.required()]],
      artifactDependance: ['', [RxwebValidators.required()]],
      versionDependance: [''],
      authentificationType: ['', [RxwebValidators.required()]],
      dependanceScope: ['', [RxwebValidators.required()]]
    });
  }

}
