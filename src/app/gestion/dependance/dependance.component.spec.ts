import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependanceComponent } from './dependance.component';

describe('DependanceComponent', () => {
  let component: DependanceComponent;
  let fixture: ComponentFixture<DependanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
