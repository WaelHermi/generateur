import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GestionComponent} from "./gestion.component";
import {ProprieteComponent} from "./propriete/propriete.component";
import {DependanceComponent} from "./dependance/dependance.component";

const routes: Routes = [{
  path: '',
  component: GestionComponent,
  children: [
    {
      path: 'propriete',
      component: ProprieteComponent,
    },
    {
      path: 'depenedance',
      component: DependanceComponent,
    },
    {
      path: '',
      redirectTo: 'depenedance',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: DependanceComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionRoutingModule { }
