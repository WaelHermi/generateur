import { Injectable } from '@angular/core';
import {API_GESTION_URLS} from "../config/api.url.config";
import {CrudService} from "../../services/curd.service";
import {ProprieteAuthentification} from "../models/propriete-authentification";

@Injectable({
  providedIn: 'root'
})
export class ProprieteAuthentificationService extends CrudService<ProprieteAuthentification> {
  url = API_GESTION_URLS.PROPRIETE_AUTHENTIFICATION_URL;
}
