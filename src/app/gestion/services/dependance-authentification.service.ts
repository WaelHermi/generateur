import { Injectable } from '@angular/core';
import {CrudService} from "../../services/curd.service";
import {DependanceAuthentification} from "../models/dependance-authentification";
import {API_GESTION_URLS} from "../config/api.url.config";

@Injectable({
  providedIn: 'root'
})
export class DependanceAuthentificationService extends CrudService<DependanceAuthentification> {
  url = API_GESTION_URLS.DEPENDANCE_AUTHENTIFICATION_URL;
}
