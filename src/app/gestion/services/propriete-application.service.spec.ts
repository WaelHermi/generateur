import { TestBed } from '@angular/core/testing';

import { ProprieteApplicationService } from './propriete-application.service';

describe('ProprieteApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProprieteApplicationService = TestBed.get(ProprieteApplicationService);
    expect(service).toBeTruthy();
  });
});
