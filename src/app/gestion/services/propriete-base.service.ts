import { Injectable } from '@angular/core';
import {Propriete} from "../models/propriete";
import {ProprieteBase} from "../models/propriete-base";
import {CrudService} from "../../services/curd.service";
import {API_GESTION_URLS} from "../config/api.url.config";

@Injectable({
  providedIn: 'root'
})
export class ProprieteBaseService extends CrudService<ProprieteBase> {
  url = API_GESTION_URLS.PROPRIETE_BASE_URL;
}
