import { TestBed } from '@angular/core/testing';

import { DependanceAuthentificationService } from './dependance-authentification.service';

describe('DependanceAuthentificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DependanceAuthentificationService = TestBed.get(DependanceAuthentificationService);
    expect(service).toBeTruthy();
  });
});
