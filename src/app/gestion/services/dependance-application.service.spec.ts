import { TestBed } from '@angular/core/testing';

import { DependanceApplicationService } from './dependance-application.service';

describe('DependanceApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DependanceApplicationService = TestBed.get(DependanceApplicationService);
    expect(service).toBeTruthy();
  });
});
