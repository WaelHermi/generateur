import {Injectable} from '@angular/core';
import {API_GESTION_URLS} from "../config/api.url.config";
import {CrudService} from "../../services/curd.service";
import {DependanceBase} from "../models/dependance-base";

@Injectable({
  providedIn: 'root'
})
export class DependanceBaseService extends CrudService<DependanceBase>{
  url = API_GESTION_URLS.DEPENDANCE_BASE_URL;
}
