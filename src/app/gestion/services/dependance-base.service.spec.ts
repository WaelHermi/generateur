import { TestBed } from '@angular/core/testing';

import { DependanceBaseService } from './dependance-base.service';

describe('DependanceBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DependanceBaseService = TestBed.get(DependanceBaseService);
    expect(service).toBeTruthy();
  });
});
