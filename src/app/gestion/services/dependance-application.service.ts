import { Injectable } from '@angular/core';
import {CrudService} from "../../services/curd.service";
import {DependanceApplication} from "../models/dependance-application";
import {API_GESTION_URLS} from "../config/api.url.config";

@Injectable({
  providedIn: 'root'
})
export class DependanceApplicationService extends CrudService<DependanceApplication> {
  url = API_GESTION_URLS.DEPENDANCE_APPLICATION_URL;
}
