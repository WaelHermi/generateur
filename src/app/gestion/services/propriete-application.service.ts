import { Injectable } from '@angular/core';
import {API_GESTION_URLS} from "../config/api.url.config";
import {CrudService} from "../../services/curd.service";
import {ProprieteApplication} from "../models/propriete-application";

@Injectable({
  providedIn: 'root'
})
export class ProprieteApplicationService extends CrudService<ProprieteApplication>{
  url = API_GESTION_URLS.PROPRIETE_APPLICATION_URL;

}
