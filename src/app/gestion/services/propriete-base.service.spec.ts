import { TestBed } from '@angular/core/testing';

import { ProprieteBaseService } from './propriete-base.service';

describe('ProprieteBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProprieteBaseService = TestBed.get(ProprieteBaseService);
    expect(service).toBeTruthy();
  });
});
