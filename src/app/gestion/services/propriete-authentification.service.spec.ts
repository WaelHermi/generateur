import { TestBed } from '@angular/core/testing';

import { ProprieteAuthentificationService } from './propriete-authentification.service';

describe('ProprieteAuthentificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProprieteAuthentificationService = TestBed.get(ProprieteAuthentificationService);
    expect(service).toBeTruthy();
  });
});
