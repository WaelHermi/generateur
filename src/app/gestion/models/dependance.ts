export class Dependance {
  idDependance: String;
  versionDependance: String;
  artifactDependance: String;
  groupDependance: String;
  dependanceScope: String;
}
