import {Propriete} from "./propriete";

export class ProprieteAuthentification extends Propriete{
  authentificationType: String;
}
