import {Component, OnInit, Input, DoCheck, IterableDiffers} from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { CrudService } from '../../../services/curd.service';
import { DataModel } from '../../../shared/data-model';
import { GridApi } from 'ag-grid-community';
import { SortableHeaderComponent } from '../../../ag-grid/sortable-header/sortable-header.component';
import { ButtonRendererComponent } from '../../button-renderer/button-renderer.component';
import { formatDate } from '@angular/common';
import {element} from "protractor";
import {ReactiveFormConfig} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-curd',
  templateUrl: './curd.component.html',
  styleUrls: ['./curd.component.scss']
})
export class CurdComponent implements OnInit, DoCheck {

  @Input()
  title: string;

  @Input()
  entity: String;

  @Input()
  service: CrudService<any>;

  @Input()
  initItem: any;

  @Input()
  idName: string = '';

  @Input()
  operationForm: FormGroup;

  @Input()
  dataModelList: DataModel[];

  @Input()
  create: Boolean = true;

  @Input()
  update: Boolean = true;

  @Input()
  delete: Boolean = true;

  @Input()
  operationTable;

  iterableDiffer;

  idValue: String;

  event: any;

  dataSetUp: Boolean = false;

  isOperationSubmitted: Boolean = false;
  displayOperationUpdate: Boolean = false;
  displayOperationDelete: Boolean = false;
  displayOperation: Boolean = false;
  nbSpinnerOperation: boolean;
  blockButton: boolean = false;

  private items: number;

  cols: any[] = [];

  formControlsOperations() { return this.operationForm.controls; }

  constructor(private iterableDiffers: IterableDiffers){
    this.iterableDiffer = this.iterableDiffers.find([]).create(null);
  }

  ngOnInit(){

    ReactiveFormConfig.set({

      "validationMessage":{

        "required":"Ce champ est requis!",

        "minLength":"La longueur minimale est de {{1}}!",

        "maxLength":"la longueur maximale autorisée est de {{1}}!",

      }

    });
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.dataModelList);
    if (changes) {
      if(this.dataModelList != null && !this.dataSetUp){
        this.dataModelList.forEach( element => {
          if(element.read){
            if(element.type == 'select'){
              const myClonedArray  = Object.assign([], element.selectModel.selectModelList);
              myClonedArray.unshift({label: '', value:''});
              this.cols.push({ field: element.columnName, header: element.columnReference, type: element.type, list:  myClonedArray});
            }else {
              this.cols.push({ field: element.columnName, header: element.columnReference, type: element.type });
            }
          }
        });
        this.dataSetUp = true;
      }
    }
  }

  dataChanged($event) {
    this.operationTable = this.operationTable.concat($event);
  }

  onCreateClick() {
    this.displayOperation = true;
    this.operationForm.reset()

    this.dataModelList.forEach( element => {
      if(element.type == 'select'){
        this.operationForm.controls[element.columnName].setValue(element.selectModel.selectModelList[0].value);
      }
    })
  }

  createOperation() {
    this.isOperationSubmitted = true;
    this.nbSpinnerOperation = true;
    this.blockButton = true;
    if(!this.operationForm.invalid){
      this.initItem = this.operationForm.value;
      this.service.add(this.initItem).subscribe( response => {
        this.operationTable.push(response);
        this.operationForm.reset();
        this.displayOperation = false
        this.isOperationSubmitted = false;
        this.nbSpinnerOperation = false;
        this.blockButton = false;
      }, error => {
        this.nbSpinnerOperation = false;
        this.blockButton = false;
      });
    }else {
      this.nbSpinnerOperation = false;
      this.blockButton = false;
    }
  }

  updateOperation(){
    this.isOperationSubmitted = true;
    this.nbSpinnerOperation = true;
    this.blockButton = true;
    if(!this.operationForm.invalid){
      this.initItem = this.operationForm.value;
      this.initItem[this.idName] = this.idValue;
      this.service.update(this.initItem).subscribe( response => {
        const index = this.operationTable.indexOf(this.event);
        this.operationTable.splice(index, 1, response)
        this.operationForm.reset();
        this.displayOperationUpdate = false
        this.isOperationSubmitted = false;
        this.nbSpinnerOperation = false;
        this.blockButton = false;
      });
    }else {
      this.isOperationSubmitted = false;
      this.nbSpinnerOperation = false;
      this.blockButton = false;
    }
  }


  deleteOperation(){
    this.nbSpinnerOperation=true;
    this.blockButton=true;
    this.service.delete(this.initItem[this.idName]).subscribe( response => {
      const index = this.operationTable.indexOf(this.event);
      this.operationTable.splice(index, 1);
      this.nbSpinnerOperation=false;
      this.blockButton=false;
      this.displayOperationDelete=false;
    });
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.operationTable = pageOfItems;
  }

  onUpdateClick(event) {
    this.idValue = event[this.idName];
    this.event = event;
    this.displayOperationUpdate=true;
    this.operationForm.patchValue(event);
  }

  onDeleteClick(event) {
    this.idValue = event[this.idName];
    this.initItem = event;
    this.event = event;
    this.displayOperationDelete=true;
    this.operationForm.patchValue(event);
  }
}
