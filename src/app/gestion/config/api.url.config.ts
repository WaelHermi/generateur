import { environment } from '../../../environments/environment';

const BASE = environment.API_IP;
const PORT = environment.API_PORT;
const VERSION = '/v1';


export const API_GESTION_URLS = {
  DEPENDANCE_APPLICATION_URL : BASE + PORT + VERSION + '/dependanceApplication',
  DEPENDANCE_AUTHENTIFICATION_URL : BASE + PORT + VERSION + '/dependanceAuthentification',
  DEPENDANCE_BASE_URL : BASE + PORT + VERSION + '/dependanceBase',
  PROPRIETE_APPLICATION_URL : BASE + PORT + VERSION + '/proprieteApplication',
  PROPRIETE_AUTHENTIFICATION_URL : BASE + PORT + VERSION + '/proprieteAuthentification',
  PROPRIETE_BASE_URL : BASE + PORT + VERSION + '/proprieteBase',
};
