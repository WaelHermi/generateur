import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../authentification/auth.service';

@Injectable()
/*
   The Error Interceptor intercepts http responses from the api to check if there were any errors.
   If there is a 401 Unauthorized response the user is automatically logged out of the application,
   all other errors are re-thrown up to the calling service so an alert with the error can be displayed on the screen.
*/
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService) { }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload(true);
            }
            const error = err.error;
            return throwError(error);
        }));
    }
}
