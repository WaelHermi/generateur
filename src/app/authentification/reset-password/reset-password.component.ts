import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilisateurService} from "../../services/utilisateur.service";
import {ResetPasswordRequest} from "../../models/reset-password-request";
import {MessageService} from "primeng/api";

@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {


  redirectDelay: number;
  showMessages: any;
  strategy: string;
  submitted: boolean;
  errors: string[];
  messages: string[];
  password:string;
  confirmepassword:string;
  matchpassword:boolean=false;
  private token: string;
  getConfigValue(key: string): any {}
  forms: any;

  constructor(private _utilisateurService: UtilisateurService, private router: Router, private _Activatedroute:ActivatedRoute, private messageService: MessageService) {
    this.token=this._Activatedroute.snapshot.paramMap.get('token');

  }

  ngOnInit() {
    this.forms = { validation:
      {
        passwordUtilisateur:
        { required: true, minLength: 4, maxLength: 20  },
        confirmepasswordUtilisateur:
        { required: true, minLength: 4, maxLength: 20  },
      }};
    }

  resetpassword(): void {
    if(this.password!=this.confirmepassword){
      this.matchpassword=true;
      return;
    }else{
      let resetPasswordRequest: ResetPasswordRequest = new ResetPasswordRequest();
      resetPasswordRequest.password=this.password;
      resetPasswordRequest.token=this.token;
      console.log(resetPasswordRequest);
      this._utilisateurService.resetPassword(resetPasswordRequest).subscribe( response => {
        if(response.result){
          this.messageService.add({
            severity: "success",
            summary: "Succès",
            detail: "Votre mot de passe à été réinitialisé avec succès",
            life: 1500,
          });
          setTimeout(() => {
              this.router.navigate(['/auth/login']);
            }
            , 1000);
        }else {
          this.messageService.add({
            severity: "error",
            summary: "Erreur",
            detail: "Token n'est pas correct",
            life: 3000,
          });
        }
      }, () => {
        this.messageService.add({
          severity: "error",
          summary: "Échec",
          detail: "Un problème est survenu",
          life: 3000,
        });
      })
    }
  //   console.log(this.utilisateur);
  //   this.clientSerivce.add(this.utilisateur).subscribe( () => {
  //     this.router.navigate(['/auth/login']);
  // });
  }
  }
