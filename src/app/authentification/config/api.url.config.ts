import { environment } from '../../../environments/environment';

const BASE = environment.API_IP;
const PORT = environment.API_PORT;
const VERSION = '/v1';

const AUTHENTICATE = '/authenticate';

export const API_AUTHENTIFICATION_URLS = {
    AUTHENTICATE_URL : BASE + PORT + AUTHENTICATE,
    INSCRIPTION_URL : BASE + PORT + VERSION + '/client'
};
