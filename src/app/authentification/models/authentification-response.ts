import { Utilisateur } from '../../models/utilisateur';

export class AuthentificationResponse {
    utilisateur: Utilisateur
    jwt: String;
}
