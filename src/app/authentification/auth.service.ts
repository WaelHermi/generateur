import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {AuthentificationRequest} from './models/authentification-request';
import {AuthentificationResponse} from './models/authentification-response';
import {API_AUTHENTIFICATION_URLS} from './config/api.url.config';
import {Router} from '@angular/router';
import {error} from "util";

// import { environment } from '@environments/environment';
// import { Auth } from '@app/_models';

// The authentication service is used to login & logout of the Angular app, it notifies other components when the user logs in & out, and allows access the currently logged in user.
@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentUserSubject: BehaviorSubject<AuthentificationResponse>;
    public currentUser: Observable<AuthentificationResponse>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<AuthentificationResponse>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): AuthentificationResponse {
    return this.currentUserSubject.value;
  }

  login(authentificationRequest: AuthentificationRequest) {
    return this.http.post<AuthentificationResponse>(API_AUTHENTIFICATION_URLS.AUTHENTICATE_URL , authentificationRequest);
  }

  loginTraitement(response){
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    localStorage.setItem('currentUser', JSON.stringify(response));
    this.currentUserSubject.next(response);
    this.router.navigate(['/generation/wizard']);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
