import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    constructor(
      private router: Router,
      private authenticationService: AuthService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      const currentUser = this.authenticationService.currentUserValue;
      if (currentUser) {

        if(currentUser.utilisateur.roles.filter( element => element.nomRole == "ROLE_ADMINISTRATEUR").length > 0 &&
        currentUser.utilisateur.roles.filter( element => element.nomRole == "ROLE_CLIENT").length == 0) {
          this.router.navigate(['/administration'], { queryParams: { returnUrl: state.url } });
        } 
        
        if(currentUser.utilisateur.roles.filter( element => element.nomRole == "ROLE_GESTIONNAIRE").length > 0) {
          this.router.navigate(['/gestion'], { queryParams: { returnUrl: state.url } });
        } 
          // logged in so return true
          return true;
      }

      // not logged in so redirect to login page with the return url
      this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
      return false;
  }
}
