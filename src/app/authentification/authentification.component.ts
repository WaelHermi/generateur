import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-authentification',
  template: `
  <nb-layout>
    <nb-layout-column>
      <nb-card>
        <nb-card-header>
        <nav class=\"navigation\"><a href="#" (click)="back()" class="link back-link" aria-label="Back"><nb-icon icon="arrow-back"></nb-icon></a></nav>
        </nb-card-header>
        <nb-card-body>
          <ngx-block>
            <router-outlet></router-outlet>
          </ngx-block>
        </nb-card-body>
      </nb-card>
    </nb-layout-column>
  </nb-layout>`,
  styleUrls: ['./authentification.component.scss'],
})
export class AuthentificationComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }

}
