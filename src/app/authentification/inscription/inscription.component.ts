import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../models/utilisateur';
import { UtilisateurService } from '../../services/utilisateur.service';
import { Router } from '@angular/router';
import {ClientService} from "../services/client.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'ngx-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss'],
})
export class InscriptionComponent implements OnInit {

  utilisateur: Utilisateur;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  submitted: boolean;
  errors: string[];
  messages: string[];
  getConfigValue(key: string): any {}
  forms: any;

  constructor(private clientSerivce: ClientService, private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.utilisateur = new Utilisateur();
    this.forms = { validation:
      { nomUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        prenomUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        adresseMailUtilisateur:
        { required: true, minLength: 4, maxLength: 100 },
        adresseUtilisateur:
        { required: false, minLength: 4, maxLength: 200 },
        telephoneUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        usernameUtilisateur:
        { required: true, minLength: 4, maxLength: 50 },
        passwordUtilisateur:
        { required: true, minLength: 4, maxLength: 50 },
      }};
    }

  register(): void {
    console.log(this.utilisateur);
    this.clientSerivce.add(this.utilisateur).subscribe( () => {
      this.messageService.add({
        severity: "success",
        summary: "Succès",
        detail: "Votre compte à été créé avec succès",
        life: 1000,
      });
      setTimeout(() => {
          this.router.navigate(['/auth/login']);
        }
        , 1000);
  }, () => {
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "La création de votre compte a échoué",
        life: 3000,
      });
    });
  }

}
