import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AuthentificationComponent } from './authentification.component';
import { SendMailResetPasswordComponent } from './send-mail-reset-password/send-mail-reset-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [{
  path: '',
  component: AuthentificationComponent,
  children: [
    {
      path: 'login',
      component: LoginComponent,
    },
    {
      path: 'register',
      component: InscriptionComponent,
    },
    {
      path: 'request-passwordUtilisateur',
      component: SendMailResetPasswordComponent,
    },
    {
      path: 'reset-passwordUtilisateur/:token',
      component: ResetPasswordComponent,
    },
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: LoginComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthentificationRoutingModule { }
