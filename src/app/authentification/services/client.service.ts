import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {API_AUTHENTIFICATION_URLS} from "../config/api.url.config";
import {Utilisateur} from "../../models/utilisateur";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ClientService  {

  url = API_AUTHENTIFICATION_URLS.INSCRIPTION_URL

  constructor(private http: HttpClient) { }

  add(entity): Observable<Utilisateur> {
    return this.http.post<Utilisateur>(this.url, entity);
  }
}
