import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthentificationRoutingModule } from './authentification-routing.module';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AuthentificationComponent } from './authentification.component';
import { NbAlertModule, NbInputModule, NbCheckboxModule, NbIconModule, NbMenuModule, NbCardModule, NbLayoutModule, NbButtonModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from '../@theme/theme.module';
import { NbAuthModule } from '@nebular/auth';
import { BlockComponent } from './block/block.component';
import { SendMailResetPasswordComponent } from './send-mail-reset-password/send-mail-reset-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";

@NgModule({
  declarations: [LoginComponent, InscriptionComponent, AuthentificationComponent, BlockComponent, SendMailResetPasswordComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    AuthentificationRoutingModule,
    NbAlertModule,
    FormsModule,
    NbInputModule,
    NbCheckboxModule,
    NbIconModule,
    NbMenuModule,
    ThemeModule,
    NbCardModule,
    NbLayoutModule,
    NbAuthModule,
    NbButtonModule,
    ToastModule,
  ],
  providers: [MessageService]
})
export class AuthentificationModule { }
