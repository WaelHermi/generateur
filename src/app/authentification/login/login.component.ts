import { Component, OnInit } from '@angular/core';
import { AuthentificationRequest } from '../models/authentification-request';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';


@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit  {

  utilisateur: AuthentificationRequest;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  errors: string[];
  messages: string[];
  submitted: boolean;
  rememberMe: boolean;
  authForm: FormGroup;
  forms: any;
  badCredentials: Boolean;

  constructor(private authentificationService: AuthService) { }
  ngOnInit(): void {
    this.utilisateur = new AuthentificationRequest();
    this.forms = {  validation:
      {
        usernameUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        passwordUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
      }};
    }

  login(): void {
    this.authentificationService.login(this.utilisateur).subscribe(response => {
      this.authentificationService.loginTraitement(response);
      this.badCredentials =  false;
      }, (error: Response) => {
        if(error.status == 403){
          this.badCredentials = true;
        }
      }
    );;
  }
  getConfigValue(key: string): any {}
}
