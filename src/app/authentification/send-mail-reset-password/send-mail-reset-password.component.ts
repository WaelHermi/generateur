import { Component, OnInit } from '@angular/core';
import { AuthentificationRequest } from '../models/authentification-request';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import {UtilisateurService} from "../../services/utilisateur.service";
import {Router} from "@angular/router";
import {MessageService} from "primeng/api";

@Component({
  selector: 'ngx-send-mail-reset-password',
  templateUrl: './send-mail-reset-password.component.html',
  styleUrls: ['./send-mail-reset-password.component.scss']
})
export class SendMailResetPasswordComponent implements OnInit {

  email: string;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  errors: string[];
  messages: string[];
  submitted: boolean;
  forms: any;

  constructor(private _utilisateurService: UtilisateurService, private messageService: MessageService) { }
  ngOnInit(): void {
    this.forms = {  validation:
      {
        emailUtilisateur:
        { required: true, minLength: 4, maxLength: 50 , email: true },
      }};
    }

  requestResetPassword(): void {
    this._utilisateurService.requestResetPassword(this.email).subscribe(  response => { if(response.result){
      this.messageService.add({
        severity: "success",
        summary: "Succès",
        detail: "Un lien à été envoyé à votre e-mail",
        life: 3000,
      });
    }else {
      this.messageService.add({
        severity: "error",
        summary: "Erreur",
        detail: "Adresse mail introuvable",
        life: 3000,
      });
    }}, error => {
      this.messageService.add({
        severity: "error",
        summary: "Erreur",
        detail: "Un problème est survenu",
        life: 3000,
      });
    });
  }
}
