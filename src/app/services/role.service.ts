import { Injectable } from '@angular/core';
import { CrudService } from './curd.service';
import { Role } from '../models/role';
import { API_MAIN_URLS } from '../config/api.url.config';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends CrudService<Role> {
  url = API_MAIN_URLS.ROLE_URL;
}
