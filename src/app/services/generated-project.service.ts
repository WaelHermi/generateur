import { Injectable } from '@angular/core';
import { CrudService } from './curd.service'
import { GeneratedProject } from '../models/generated-project';
import { API_MAIN_URLS } from '../config/api.url.config';

@Injectable({
  providedIn: 'root'
})
export class GeneratedProjectService extends CrudService<GeneratedProject>  {
  url = API_MAIN_URLS.GENERATED_PROJECT_URL;
}
