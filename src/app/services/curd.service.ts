import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Result } from '../models/Result';

@Injectable({
  providedIn: 'root',
})
export class CrudService<T> {

  url: string;
  photoUrl: string;

  constructor(protected http: HttpClient) {
  }


  getAll(size: Number, page: Number): Observable<T[]> {
    return this.http.get<T[]>(this.url + 's' + '?page=' + page + '&size=' + size);
  }

  getById(id): Observable<T> {
    return this.http.get<T>(this.url + '/' + id);
  }

  getByUtilisateur(): Observable<T[]> {
    return this.http.get<T[]>(this.url + 's' + '/getByUtilisateur');
  }

  add(entity): Observable<T> {
    return this.http.post<T>(this.url, entity);
  }

  update(entity): Observable<T> {
    return this.http.put<T>(this.url, entity);
  }

  delete(id): Observable<Result> {
    return this.http.delete<Result>(this.url + `/${id}`);
  }

  count(): Observable<Result> {
    return this.http.get<Result>(this.url + 'Count');
  }
}
