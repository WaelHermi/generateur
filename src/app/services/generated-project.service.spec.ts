import { TestBed } from '@angular/core/testing';

import { GeneratedProjectService } from './generated-project.service';

describe('GeneratedProjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneratedProjectService = TestBed.get(GeneratedProjectService);
    expect(service).toBeTruthy();
  });
});
