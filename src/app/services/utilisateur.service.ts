import { Injectable } from '@angular/core';
import { CrudService } from './curd.service';
import { API_MAIN_URLS } from '../config/api.url.config';
import { Utilisateur } from '../models/utilisateur';
import {Observable} from "rxjs";
import {ResetPasswordRequest} from "../models/reset-password-request";
import {Result} from "../models/Result";
import {PasswordModel} from "../models/password-model";

@Injectable({
  providedIn: 'root',
})
export class UtilisateurService extends CrudService<Utilisateur> {
  url = API_MAIN_URLS.UTILISATEUR_URL;

  requestResetPassword(email): Observable<Result> {
    console.log(API_MAIN_URLS.REQUEST_RESET_PASSWORD_URL + '/' + email)
    return this.http.post<Result>(API_MAIN_URLS.REQUEST_RESET_PASSWORD_URL + '/' + email, email);
  }

  resetPassword(resetPasswordRequest: ResetPasswordRequest): Observable<Result> {
    return this.http.put<Result>( API_MAIN_URLS.RESET_PASSWORD_URL, resetPasswordRequest);
  }

  updatePassword(passwordModel: PasswordModel): Observable<Result> {
    return this.http.put<Result>( API_MAIN_URLS.UPDATE_PASSWORD_URL, passwordModel);
  }
}
