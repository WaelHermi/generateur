import { SelectModel } from './select-model'

export class DataModel {
    constructor(public columnName?: string,
                public columnReference?: string,
                public type?: string,
                public read?: boolean,
                public write?: boolean,
                public messages?: any,
                public required?: Boolean,
                public selectModel?: SelectModel){}
}
