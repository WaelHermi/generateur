import { TestBed } from '@angular/core/testing';

import { GenerateurService } from './generateur.service';

describe('GenerateurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenerateurService = TestBed.get(GenerateurService);
    expect(service).toBeTruthy();
  });
});
