import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_GENERATION_URLS } from '../config/api.url.config';
import { EnumWrapper } from '../models/enum-wrapper';

@Injectable({
  providedIn: 'root',
})
export class TypeService {

  constructor(protected http: HttpClient) { }

  getAllApplicationTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_APPLICATION_URL);
  }

  getAllPackgingTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_PACKAGING_URL);
  }

  getAllBaseTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_BASE_URL);
  }

  getAllAttributTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_ATTRIBUT_URL);
  }

  getAllVisibiliteTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_VISIBILITE_URL);
  }

  getAllGenerationTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_GENERATION_URL);
  }

  getAllRelationJPATypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_RELATION_URL);
  }

  getAllFetchTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_FETCH_URL);
  }

  getAllCascadeTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_CASCADE_URL);
  }

  getAllAuthentificationTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_AUTHENTIFICATION_URL);
  }

  getAllInheritanceTypes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_INHERITANCE_URL);
  }

  getAllDependanceScopes(): Observable<EnumWrapper[]> {
    return this.http.get<EnumWrapper[]>(API_GENERATION_URLS.TYPE_SCOPE_URL);
  }
}
