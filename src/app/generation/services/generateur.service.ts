import { Injectable } from '@angular/core';
import { Application } from '../models/application';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_GENERATION_URLS } from '../config/api.url.config';
import { Entity } from '../models/entity';
import { Observable } from 'rxjs';
import { Base } from '../models/base';
import { Result } from '../../models/Result';
import { UpdateProjectContext } from '../models/update-project-context';
import { GeneratedProject } from '../../models/generated-project';


@Injectable({
  providedIn: 'root',
})
export class GenerateurService {

  constructor(protected http: HttpClient) { }

  generateApplication(application: Application) {
    return this.http.post(API_GENERATION_URLS.GENERATEUR_APPLICATION_URL, application, {
      responseType: 'arraybuffer',
      headers: new HttpHeaders().append('Content-Type', 'application/json'),
    });
  }

  enregistrerApplication(application: Application) {
    return this.http.post<GeneratedProject>(API_GENERATION_URLS.ENREGITRER_APPLICATION_URL, application);
  }

  modifierEnregistrermentApplication(updateProjectContext: UpdateProjectContext) {
    return this.http.post<GeneratedProject>(API_GENERATION_URLS.MODIFIER_ENREGITREMENT_APPLICATION_URL, updateProjectContext);
  }

  getAllDatabaseTables(base: Base): Observable<Entity[]> {
    return this.http.post<Entity[]>(API_GENERATION_URLS.GENERATEUR_DATABASE_TABLES_URL, base);
  }

  getDatabaseConnection(base: Base): Observable<Result> {
    return this.http.post<Result>(API_GENERATION_URLS.CONNECTIVITY_DATABASE_URL, base);
  }

  getSavedApplication(generatedProject: GeneratedProject): Observable<Application> {
    return this.http.post<Application>(API_GENERATION_URLS.SAVED_APPLICATION_URL,generatedProject);
  }

}
