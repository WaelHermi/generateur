import { environment } from '../../../environments/environment';

const BASE = environment.API_IP;
const PORT = environment.API_PORT;
const VERSION = '/v1';

const TYPE = '/type';
const GENERATEUR = '/generateur';

export const API_GENERATION_URLS = {

  TYPE_APPLICATION_URL : BASE + PORT + VERSION + TYPE + '/application',
  TYPE_PACKAGING_URL : BASE + PORT + VERSION + TYPE + '/packaging',
  TYPE_BASE_URL : BASE + PORT + VERSION + TYPE + '/base',
  TYPE_ATTRIBUT_URL : BASE + PORT + VERSION + TYPE + '/attribut',
  TYPE_VISIBILITE_URL : BASE + PORT + VERSION + TYPE + '/visibilite',
  TYPE_GENERATION_URL : BASE + PORT + VERSION + TYPE + '/generation',
  TYPE_RELATION_URL : BASE + PORT + VERSION + TYPE + '/relation',
  TYPE_FETCH_URL : BASE + PORT + VERSION + TYPE + '/fetch',
  TYPE_CASCADE_URL : BASE + PORT + VERSION + TYPE + '/cascade',
  TYPE_AUTHENTIFICATION_URL : BASE + PORT + VERSION + TYPE + '/authentification',
  TYPE_INHERITANCE_URL : BASE + PORT + VERSION + TYPE + '/inheritance',
  TYPE_SCOPE_URL : BASE + PORT + VERSION + TYPE + '/scope',
  GENERATEUR_APPLICATION_URL : BASE + PORT + VERSION + GENERATEUR + '/application',
  ENREGITRER_APPLICATION_URL : BASE + PORT + VERSION + GENERATEUR + '/enregistrer',
  MODIFIER_ENREGITREMENT_APPLICATION_URL : BASE + PORT + VERSION + GENERATEUR + '/modifier_enregistrement',
  GENERATEUR_DATABASE_TABLES_URL : BASE + PORT + VERSION + GENERATEUR + '/database_tables',
  CONNECTIVITY_DATABASE_URL : BASE + PORT + VERSION + GENERATEUR + '/database_connection',
  SAVED_APPLICATION_URL : BASE + PORT + VERSION + GENERATEUR + '/saved_application'

};
