import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ThemeModule} from '../@theme/theme.module';
import {CommonModule} from '@angular/common';
import {WizardComponent} from './wizard/wizard.component';
import {GenerationRoutingModule} from './generation-routing/generation-routing.module';
import {GenerationComponent} from './generation.component';
import {Ng2SmartTableModule} from './ng2-smart-table/ng2-smart-table.module';
import {ShowcaseDialogComponent} from './showcase-dialog/showcase-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ToastModule} from 'primeng/toast';
import {DataViewModule} from 'primeng/dataview';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {ToolbarModule} from 'primeng/toolbar';

import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbMenuModule,
  NbRadioModule,
  NbRouteTabsetModule,
  NbSelectModule,
  NbSpinnerModule,
  NbStepperModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
  NbWindowModule
} from '@nebular/theme';
import {MessageService} from 'primeng/api';
import {RxReactiveFormsModule} from "@rxweb/reactive-form-validators";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NbMenuModule,
    ThemeModule,
    GenerationRoutingModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    Ng2SmartTableModule,
    NbDialogModule.forChild(),
    MatDialogModule,
    ToastModule,
    DataViewModule,
    PanelModule,
    DialogModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    ButtonModule,
    CodeHighlighterModule,
    NbWindowModule,
    ToolbarModule,
    NbTooltipModule,
    NbSpinnerModule,
    RxReactiveFormsModule
  ],
  declarations: [GenerationComponent, WizardComponent, ShowcaseDialogComponent],
  entryComponents: [ ShowcaseDialogComponent ],
  providers: [MessageService]
})
export class GenerationModule { }
