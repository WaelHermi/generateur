import { Entity } from './entity';

export class Relation {

    nomRelation: String;
    biNomRelation: String;
    relationJPAType: String;
    nomEntity: String;
    source: Entity;
    destination: Entity;
    bidirectionnelle: Boolean;
    cascadeTypes: String[];
    fetchType: String;
    findBy: Boolean;
    jsonIgnore: Boolean;
    dtoRelation: Boolean;

    constructor() {}
}
