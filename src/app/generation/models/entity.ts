import { ClePrimaire } from './cle-primaire';
import { Attribut } from './attribut';
import { Relation } from './relation';

export class Entity {

    id: number;
    nomEntity: String;
    clePrimaire: ClePrimaire = new ClePrimaire();
    attributs: Attribut[] = [];
    relations: Relation[] = [];
    generateControleur: Boolean;
    nomMere: String;
    mere: Entity;
    inheritanceType: String= "NON";
    discriminatorColumn: String;
    discriminatorValue: String;
    primaryKeyJoinColumn: String;
}
