import { Attribut } from './attribut';

export class ClePrimaire {
    nomClePrimaire: String;
    attributs: Attribut[] = [];
}
