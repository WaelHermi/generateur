export class Attribut {
    nomAttribut: String;
    visibiliteAttribut: String;
    attributType: String;
    unique: Boolean;
    nullable: Boolean;
    generationType: String;
    findBy: Boolean;
    dtoAttribut: Boolean;
}
