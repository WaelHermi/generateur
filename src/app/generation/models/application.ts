import {Entity} from './entity';
import {Base} from './base';

export class Application {

    groupApplication: String;
    artifactApplication: String;
    versionApplication: String;
    descriptionApplication: String = "Description de l'application";
    portApplication: String;
    packagingApplication: String;
    applicationType: String = "MONOLITHIQUE";
    authentificationType: String;
    lombok: Boolean = false;
    springVersion: String;
    javaVersion: String;
    entities: Entity[];
    base: Base;

}
