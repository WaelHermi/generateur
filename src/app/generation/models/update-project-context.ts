import { Application } from './application';
import { GeneratedProject } from '../../models/generated-project';

export class UpdateProjectContext {
    constructor(
        public application: Application, 
        public generatedProject: GeneratedProject
        ){}
}
