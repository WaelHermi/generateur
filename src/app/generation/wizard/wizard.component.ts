import {Component, DoCheck, IterableDiffers, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {TypeService} from '../services/type.service';
import {EnumWrapper} from '../models/enum-wrapper';

import {Application} from '../models/application';
import {Base} from '../models/base';
import {Entity} from '../models/entity';
import {GenerateurService} from '../services/generateur.service';
import {NbDialogService} from '@nebular/theme';
import {ShowcaseDialogComponent} from '../showcase-dialog/showcase-dialog.component';
import {TABLE_SETTINGS} from './table-settings';
import {ClePrimaire} from '../models/cle-primaire';
import {Result} from '../../models/Result';
import {MessageService} from 'primeng/api';
import {GeneratedProject} from '../../models/generated-project';
import {UpdateProjectContext} from '../models/update-project-context';
import {GeneratedProjectService} from '../../services/generated-project.service';
import {ReactiveFormConfig, RxFormBuilder, RxwebValidators} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit, DoCheck {


  generatedProjects: GeneratedProject[] = [];
  application: Application = null;
  connectivity: Result = { result: null };
  buttonClicked: Boolean = false;
  extractionState: Boolean = false;

  // Forms
  applicationForm: FormGroup;
  baseForm: FormGroup;
  secondForm: FormGroup;

  // Types
  applicationTypes: EnumWrapper[] = [];
  packagingTypes: EnumWrapper[] = [];
  baseTypes: EnumWrapper[] = [];
  attributTypes: EnumWrapper[] = [];
  visibiliteTypes: EnumWrapper[] = [];
  generationTypes: EnumWrapper[] = [];
  relationJPATypes: EnumWrapper[] = [];
  fetchTypes: EnumWrapper[] = [];
  cascadeTypes: EnumWrapper[] = [];
  authentificationTypes: EnumWrapper[] = [];
  versionTypes: String[] = ['SNAPSHOT', 'RELEASE'];
  booleanTypes: { nom: String, valeur: String }[] = [ { nom: "Oui", valeur: "true"}, { nom: "Non", valeur: "false"} ];
  versionType: String = this.versionTypes[0];

  generatedProject: GeneratedProject;
  source: Entity[] = [];

  iterableDiffer;
  settings = TABLE_SETTINGS.DEFAULT_ENTITY_SETTING;
  selectedRows: Entity[];
  displayOperationDelete: Boolean;

  nbSpinner: boolean = false;


  constructor(private fb: RxFormBuilder, private typeService: TypeService,
    private generateurService: GenerateurService,
    private iterableDiffers: IterableDiffers, private dialogService: NbDialogService,
    private messageService: MessageService,
    private generatedProjectService: GeneratedProjectService) {
      // const data = this.service.getData();
      // this.source.load(data);
      this.iterableDiffer = this.iterableDiffers.find([]).create(null);
    }

  ngOnInit() {

    ReactiveFormConfig.set({

      "validationMessage":{

        "required":"Ce champ est requis!",

        "minLength":"La longueur minimale est de {{1}}!",

        "maxLength":"la longueur maximale autorisée est de {{1}}!",

      }

    });

    this.getGeneratedProjects();

    this.applicationForm = this.fb.group({
      groupCtrl: ['com.example' , [RxwebValidators.required()]],
      artifactCtrl: ['demo', [RxwebValidators.required()]],
      portCtrl: ['9090', [RxwebValidators.required()]],
      versionCtrl: ['0.0.1', [RxwebValidators.required()]],
      authCtrl: ["NON", [RxwebValidators.required()]],
      lombok: ["true"],
      descriptionCtrl: ["Description de l'application", [RxwebValidators.required()]]
    });


    this.baseForm = this.fb.group({
      nameCtrl: ['myDataBase', [RxwebValidators.required()]],
      usernameCtrl: ['root', [RxwebValidators.required()]],
      passwordCtrl: ['root', [RxwebValidators.required()]],
      ipCtrl: ['127.0.0.1', [RxwebValidators.required()]],
      portCtrl: ['3306', [RxwebValidators.required()]],
    });

    this.typeService.getAllApplicationTypes().subscribe(
      data => this.applicationTypes = data
    );

    this.typeService.getAllPackgingTypes().subscribe(
      data => { this.packagingTypes = data; },
    );

    this.typeService.getAllAuthentificationTypes().subscribe(
      data => { this.authentificationTypes = data; },
    );

    this.typeService.getAllBaseTypes().subscribe(
      data => this.baseTypes = data,
    );

    this.typeService.getAllAttributTypes().subscribe(
      data =>  this.attributTypes = data
    );

    this.typeService.getAllVisibiliteTypes().subscribe(
      data => this.visibiliteTypes = data,
    );

    this.typeService.getAllGenerationTypes().subscribe(
      data => this.generationTypes = data,
    );

    this.typeService.getAllRelationJPATypes().subscribe(
      data => this.relationJPATypes = data,
    );

    this.typeService.getAllFetchTypes().subscribe(
      data => this.fetchTypes = data,
    );

    this.typeService.getAllCascadeTypes().subscribe(
      data => this.cascadeTypes = data,
    );

    this.settings.columns.nomMere.editor.config.list.push({title: 'Object', value: 'Object'});

  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.source);
    if (changes) {
        this.settings.columns.nomMere.editor.config.list = this.source.map(element => ({ title: element.nomEntity, value: element.nomEntity }));
        this.settings = Object.assign({}, this.settings);
        this.settings.columns.nomMere.editor.config.list.push({title: 'Object', value: 'Object'});
    }
  }


  // Stepper
  selectGeneratedProject(generatedProject) {
    this.nbSpinner = true;
    this.generatedProject = generatedProject;
    this.application = new Application();
    this.extractionState = false;
    this.buttonClicked = false;
    this.connectivity = { result: null}
    this.generateurService.getSavedApplication(generatedProject).subscribe( response =>
      {
        this.application = response;
        this.applicationForm.controls["groupCtrl"].setValue(this.application.groupApplication);
        this.applicationForm.controls["artifactCtrl"].setValue(this.application.artifactApplication);
        this.applicationForm.controls["portCtrl"].setValue(this.application.portApplication);
        let version = this.application.versionApplication.replace('-SNAPSHOT', '');
        version = version.replace('-RELEASE', '');
        this.versionType = this.application.versionApplication.includes("SNAPSHOT") ? 'SNAPSHOT' : 'RELEASE'
        this.applicationForm.controls["versionCtrl"].setValue(version);
        this.applicationForm.controls["authCtrl"].setValue(this.application.authentificationType);
        this.applicationForm.controls["lombok"].setValue(String(this.application.lombok));
        this.applicationForm.controls["descriptionCtrl"].setValue(this.application.descriptionApplication);

        this.baseForm.controls["nameCtrl"].setValue(this.application.base.nomBase);
        this.baseForm.controls["usernameCtrl"].setValue(this.application.base.usernameBase);
        this.baseForm.controls["passwordCtrl"].setValue(this.application.base.passwordBase);
        this.baseForm.controls["ipCtrl"].setValue(this.application.base.ipBase);
        this.baseForm.controls["portCtrl"].setValue(this.application.base.portBase);
        this.source = this.application.entities;
        this.nbSpinner = false;
    }, (error) => {                              //Error callback
      console.error('error caught in component')
      console.log(error);
      this.nbSpinner = false;
    });
  }

  affectApplicationValues() {
    this.applicationForm.markAsDirty();
    const formValue = this.applicationForm.value;
    this.application.groupApplication = formValue.groupCtrl;
    this.application.artifactApplication = formValue.artifactCtrl;
    this.application.portApplication = formValue.portCtrl;
    this.application.versionApplication = formValue.versionCtrl + '-' + this.versionType;
    this.application.lombok = Boolean(formValue.lombok);
    this.application.descriptionApplication = formValue.descriptionCtrl;
  }

  onApplicationSubmit(stepper) {

    if(this.applicationForm.valid){
      this.affectApplicationValues()
      this.onSaveAPIConfiguration(stepper);
      if(this.application.base == null){
        this.application.base = new Base();
      }
      if (this.baseTypes != null && this.baseTypes.length > 0) {
        this.application.base.baseType = this.baseTypes[0].nomEnum;
      }
    }
  }

  affectBaseValues() {
    this.baseForm.markAsDirty();
    const formValue = this.baseForm.value;
    this.application.base.nomBase = formValue.nameCtrl;
    this.application.base.usernameBase = formValue.usernameCtrl;
    this.application.base.passwordBase = formValue.passwordCtrl;
    this.application.base.ipBase = formValue.ipCtrl;
    this.application.base.portBase = formValue.portCtrl;
  }

  onBaseSubmit(stepper) {
    if(this.baseForm.valid){
      this.affectBaseValues();
      console.log(this.application);
      this.onSaveAPIConfiguration(stepper);
      if(this.selectedRows == null){
        this.selectedRows = [];
      }
      if(this.source == null){
        this.source = [];
      }
      if(this.extractionState) {
        this.generateurService.getAllDatabaseTables(this.application.base).subscribe(data => {
          data.forEach( dataElement =>  {
            let sourceEntity: Entity = this.source.find(sourceElement => sourceElement.nomEntity == dataElement.nomEntity);
            if(sourceEntity == null){
              this.source.push(dataElement);
            }
            else {
              const index = this.source.indexOf(sourceEntity);
              this.source.splice(index, 1, dataElement);
            }
          })
        })
      }
    }
  }

  affectEntitiesValues() {
    this.application.entities = this.selectedRows;
  }

  onThirdSubmit(stepper) {
    this.generateApplicationThirdStep();
    this.onSaveAPIConfiguration(stepper);
    this.restAll();
  }

  generateApplicationFirstStep() {
    this.affectApplicationValues();
    if(this.selectedRows != null){
      this.selectedRows.forEach( rowElement => { if( rowElement.relations != null) {
        rowElement.relations.forEach( relationElement => {
          if (this.selectedRows.find(filterRowElement => filterRowElement.nomEntity == relationElement.nomEntity) == null) {
            const index: number = rowElement.relations.indexOf(relationElement);
            rowElement.relations.splice(index, 1);
          }
        })
      }});
    }
    console.log(this.application);
    this.generateurService.generateApplication(this.application).subscribe(data => {
      const blob = new Blob([data], {
        type: 'application/zip',
      });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
    this.restAll();
  }

  generateApplicationSecondStep() {
    this.affectBaseValues();
    if(this.selectedRows != null){
      this.selectedRows.forEach( rowElement => { if( rowElement.relations != null) {
        rowElement.relations.forEach( relationElement => {
          if (this.selectedRows.find(filterRowElement => filterRowElement.nomEntity == relationElement.nomEntity) == null) {
            const index: number = rowElement.relations.indexOf(relationElement);
            rowElement.relations.splice(index, 1);
          }
        })
      }});
    }
    console.log(this.application);
    this.generateurService.generateApplication(this.application).subscribe(data => {
      const blob = new Blob([data], {
        type: 'application/zip',
      });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
    this.restAll();
  }


  generateApplicationThirdStep() {
    this.affectEntitiesValues();
    if(this.selectedRows != null){
      this.selectedRows.forEach( rowElement => { if( rowElement.relations != null) {
        rowElement.relations.forEach( relationElement => {
          if (this.selectedRows.find(filterRowElement => filterRowElement.nomEntity == relationElement.nomEntity) == null) {
            const index: number = rowElement.relations.indexOf(relationElement);
            rowElement.relations.splice(index, 1);
          }
        })
      }});
    }
    console.log(this.application);
    this.generateurService.generateApplication(this.application).subscribe(data => {
      const blob = new Blob([data], {
        type: 'application/zip',
      });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  onNewApi(){

    this.nbSpinner = true;
    this.generatedProject = null;
    this.application = new Application();
    this.application.base = null;
    this.source = [];
    this.extractionState = false;
    this.buttonClicked = false;
    this.connectivity = { result: null}

    this.applicationForm = this.fb.group({
      groupCtrl: ['com.example' , [RxwebValidators.required()]],
      artifactCtrl: ['demo', [RxwebValidators.required()]],
      portCtrl: ['9090', [RxwebValidators.required()]],
      versionCtrl: ['0.0.1', [RxwebValidators.required()]],
      authCtrl: ["NON", [RxwebValidators.required()]],
      lombok: ["true"],
      descriptionCtrl: ["Description de l'application"]
    });


    this.baseForm = this.fb.group({
      nameCtrl: ['myDataBase', [RxwebValidators.required()]],
      usernameCtrl: ['root', [RxwebValidators.required()]],
      passwordCtrl: ['root', [RxwebValidators.required()]],
      ipCtrl: ['127.0.0.1', [RxwebValidators.required()]],
      portCtrl: ['3306', [RxwebValidators.required()]],
    });

    this.application.packagingApplication = this.packagingTypes[0].nomEnum;
    this.application.authentificationType = this.authentificationTypes[0].nomEnum;
    this.nbSpinner = false;
  }

  onSaveAPIConfiguration(stepper?) {
    this.nbSpinner = true;
    if(this.generatedProject != null){
      const saveApplication: Application = Object.assign({}, this.application);
      saveApplication.entities = this.source;
      let updateProjectContext: UpdateProjectContext = new UpdateProjectContext(saveApplication, this.generatedProject);
      this.generateurService.modifierEnregistrermentApplication(updateProjectContext).subscribe( response => { this.generatedProject = response
        this.generatedProject = response;
        this.messageService.add({
          severity: "success",
          summary: "Succès du sauvegarde",
          detail: "Le sauvegarde de votre API a été modifié avec succès",
          life: 3000,
        });
        if(stepper != null) {
          stepper.next();
        }
        this.getGeneratedProjects();
        this.nbSpinner = false;
      }, error => {
        this.nbSpinner = false;
      });
    }else {
      console.log(this.application)
      this.generateurService.enregistrerApplication(this.application).subscribe( response => {
        this.generatedProject = response;
        this.messageService.add({
          severity: "success",
          summary: "Succès du sauvegarde",
          detail: "La configuartion de votre API a été sauvegardé avec succès",
          life: 3000,
        });
        if(stepper != null) {
          stepper.next();
        }
        this.getGeneratedProjects();
        this.nbSpinner = false;
      }, error => {
        this.nbSpinner = false;
      });
    }
  }

  reset(stepper) {
    stepper.reset();
  }

  // Entités

  onRelationUpdate(event): void {

    const settings = TABLE_SETTINGS.DEFAULT_RELATION_SETTING;
    const entity = this.source[this.source.indexOf(event.data)];

    settings.columns.nomEntity.editor.config.list = this.source.map(element => ({ value: element.nomEntity, title: element.nomEntity }));

    settings.columns.relationJPAType.editor.config.list = this.relationJPATypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));

    settings.columns.fetchType.editor.config.list = this.fetchTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));
      this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        id: 3,
        title: 'Les relations de la classe ' + entity.nomEntity,
        settings: settings,
        source: entity.relations,
        types: this.cascadeTypes,
      },
    });

    }

  onAttributUpdate(event): void {

    const settings = TABLE_SETTINGS.DEFAULT_ATTRIBUT_SETTING;
    settings.columns.attributType.editor.config.list = this.attributTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));
    settings.columns.visibiliteAttribut.editor.config.list = this.visibiliteTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));

      const entity = this.source[this.source.indexOf(event.data)];
      entity.attributs = entity.attributs == null ? [] : entity.attributs;
      this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        id: 2,
        title: 'Les attributs de la classe ' + entity.nomEntity,
        settings: settings,
        source: entity.attributs,
      },
    });

  }

  onPrimaryKeyUpdate(event): void {

    const settings = TABLE_SETTINGS.DEFAULT_PRIMARY_KEY_SETTING;
    settings.columns.attributType.editor.config.list = this.attributTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));
    settings.columns.visibiliteAttribut.editor.config.list = this.visibiliteTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));
    settings.columns.generationType.editor.config.list = this.generationTypes.map(element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));

      const entity = this.source[this.source.indexOf(event.data)];
      entity.clePrimaire = entity.clePrimaire == null ? new ClePrimaire() : entity.clePrimaire;
      this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        id: 1,
        title: 'La clé primaire de la classe ' + entity.nomEntity,
        settings: settings,
        source: entity.clePrimaire.attributs,
      },
    });
  }

  onAdvancedUpdate(event): void {

    let entity: Entity = event.data;
    entity.mere = this.source.filter( element => element.nomEntity == entity.nomMere)[0];

    this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        id: 4,
        title: 'Options d\'héritage',
        advanced: true,
        entity: entity,
        entities: this.source
      },
    });
  }


  onCreateEntity(event): void {
    const entity = new Entity();
    if(event.newData.nomEntity == null || event.newData.nomEntity == ''){
      this.messageService.add({
      severity: "error",
      summary: "Échec",
      detail: "Le nom de l'entité est obligatoire",
      life: 3000,
    });
    }
    else if(this.source.filter( element => element.nomEntity != null && event.newData.nomEntity != null && element.nomEntity.toUpperCase() == event.newData.nomEntity.toUpperCase()).length > 0){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "L'entité " +  event.newData.nomEntity  + " existe déjà ",
        life: 3000,
      });
    }
    else{
      entity.nomEntity = event.newData.nomEntity;
      entity.nomMere = event.newData.nomMere;
      entity.generateControleur = event.newData.generateControleur;
      this.source.push(entity);
    }
  }

  onEditEntity(event): void {
    if(event.newData.nomEntity == null || event.newData.nomEntity == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Le nom de l'entité est obligatoire",
        life: 3000,
      });
    }
    else if(this.source.filter( element =>  element.nomEntity != null && event.newData.nomEntity != null && element.nomEntity.toUpperCase() == event.newData.nomEntity.toUpperCase()).length > 0 && event.data.nomEntity.toUpperCase() != event.newData.nomEntity.toUpperCase()){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "L'entité " +  event.newData.nomEntity  + " existe déjà ",
        life: 3000,
      });
    }else {
      event.confirm.resolve()
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Etes-vous sûr que vous voulez le supprimer')) {
      const index = this.source.indexOf(event.data);
      this.source.filter( element =>  element.nomMere == event.data.nomEntity ).forEach( element => {
        element.nomMere = 'Object';
        element.mere = null;
       });
      this.source.forEach( entity =>  {
        if(entity.relations != null){
          entity.relations = entity.relations.filter( relation => relation.nomEntity != event.data.nomEntity ); }
        })
      this.source.splice(index, 1);
    } else {
      event.confirm.reject();
    }
  }

  testConnectivity(){

    const formValue = this.baseForm.value;
    this.application.base.nomBase = formValue.nameCtrl;
    this.application.base.usernameBase = formValue.usernameCtrl;
    this.application.base.passwordBase = formValue.passwordCtrl;
    this.application.base.ipBase = formValue.ipCtrl;
    this.application.base.portBase = formValue.portCtrl;

    this.generateurService.getDatabaseConnection(this.application.base).subscribe( response => {
      this.connectivity = response;
    })
  }

  getGeneratedProjects() {
    this.generatedProjectService.getByUtilisateur().subscribe( response => this.generatedProjects = response )
  }

  public onUserRowSelect(event) {
    this.selectedRows = event.selected;
  }

  deleteGeneratedProject(generatedProject: GeneratedProject) {
    this.displayOperationDelete = true;
    this.generatedProject = generatedProject;

  }

  confirmDeleteGeneratedProject() {
    this.generatedProjectService.delete(this.generatedProject.idGeneratedProject).subscribe( response => {
      if(response.result == true){
        this.messageService.add({
          severity: "success",
          summary: "Succès de la suppression",
          detail: "La suppression de votre API a été faite avec succès",
          life: 3000,
        });
        const index = this.generatedProjects.indexOf(this.generatedProject);
        this.generatedProjects.splice(index, 1);
        this.generatedProject = null;
        this.displayOperationDelete = false;
      }
    } );
  }

  restAll() {
    this.generatedProject = null;
    this.application = null;
    this.extractionState = false;
    this.buttonClicked = false;
    this.connectivity = { result: null}
  }
}
