
export const TABLE_SETTINGS = {

    DEFAULT_ENTITY_SETTING : {
      selectMode: 'multi',
        actions: {
          position: 'right',
          relation: true,
          attribut: true,
          primaryKey: true,
          advanced: true,
        },
        relation: {
          relationButtonContent: '<i class="nb-arrow-retweet"</i>',
        },
        attribut: {
          attributButtonContent: '<i class="nb-list"</i>',
        },
        primaryKey: {
          primaryKeyButtonContent: '<nb-icon icon="star"></nb-icon>',
        },
        advanced: {
          advancedButtonContent: '<i class="nb-list"</i>',
        },
        add: {
          addButtonContent: '<i class="nb-plus"</i>',
          createButtonContent: '<i class="nb-checkmark"></i>',
          cancelButtonContent: '<i class="nb-close"></i>',
          confirmCreate: true,
        },
        edit: {
          editButtonContent: '<i class="nb-edit"></i>',
          saveButtonContent: '<i class="nb-checkmark"></i>',
          cancelButtonContent: '<i class="nb-close"></i>',
          confirmSave: true,
        },
        delete: {
          deleteButtonContent: '<i class="nb-trash"></i>',
          confirmDelete: true,
        },
        columns: {
          nomEntity: {
            title: 'Nom entité',
            type: 'string',
          },
          nomMere: {
            title: 'Classe mère',
            type: 'string',
            defaultValue: 'Object',
            editable: false,
            editor: {
              type: 'list',
              config: {
                list: [],
              },
            },
          },
          generateControleur: {
            title: 'Contrôleur',
            type: 'string',
            defaultValue: true,
            editor: {
              type: 'list',
              config: {
                list: [{title: 'Oui', value: true},
                {title: 'Non', value: false},
              ],
              },
            },
          },
        },
      },
      DEFAULT_PRIMARY_KEY_SETTING : {
        actions: {
          position: 'right',
        },
        add: {
          addButtonContent: '<i class="nb-plus"</i>',
          createButtonContent: '<i class="nb-checkmark"></i>',
          cancelButtonContent: '<i class="nb-close"></i>',
          confirmCreate: true,
        },
        edit: {
          editButtonContent: '<i class="nb-edit"></i>',
          saveButtonContent: '<i class="nb-checkmark"></i>',
          cancelButtonContent: '<i class="nb-close"></i>',
          confirmSave: true,
        },
        delete: {
          deleteButtonContent: '<i class="nb-trash"></i>',
          confirmDelete: true,
        },
        columns: {
          nomAttribut: {
            title: 'Nom attribut',
            type: 'string',
          },
          attributType: {
            title: 'Type attribut',
            type: 'string',
            defaultValue: 'UUID',
            editor: {
              type: 'list',
              config: {
                list: [],
              },
            },
          },
          visibiliteAttribut: {
            title: 'Visibilité',
            type: 'string',
            defaultValue: 'PRIVATE',
            editor: {
              type: 'list',
              config: {
                list: [],
              },
            },
          },
          generationType: {
            title: 'Type de génération',
            type: 'string',
            defaultValue: 'NON',
            editor: {
              type: 'list',
              config: {
                list: [],
              },
            },
          },
        },
    },
    DEFAULT_ATTRIBUT_SETTING : {
      actions: {
        position: 'right',
      },
      add: {
        addButtonContent: '<i class="nb-plus"</i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        nomAttribut: {
          title: 'Nom attribut',
          type: 'string',
        },
        attributType: {
          title: 'Type attribut',
          type: 'string',
          defaultValue: 'STRING',
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
        visibiliteAttribut: {
          title: 'Visibilité',
          type: 'string',
          defaultValue: 'PRIVATE',
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
        unique: {
          title: 'Unique',
          type: 'string',
          defaultValue: false,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        nullable: {
          title: 'Nullable',
          type: 'string',
          defaultValue: true,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        dtoAttribut: {
          title: 'Ajout au DTO',
          type: 'string',
          defaultValue: true,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        findBy: {
          title: 'findBy',
          type: 'string',
          defaultValue: false,
          editor: {
            type: 'list',
            config: {
              list: [
              {title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
      },
    },
    DEFAULT_RELATION_SETTING : {
      actions: {
        position: 'right',
        cascade: true,
      },
      cascade: {
        cascadeButtonContent: '<i class="nb-arrow-retweet"</i>',
      },
      add: {
        addButtonContent: '<i class="nb-plus"</i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        nomEntity: {
          title: 'Classe',
          type: 'string',
          defaultValue: null,
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
        nomRelation: {
          title: 'Nom source',
          type: 'string',
        },
        biNomRelation: {
          title: 'Nom destination',
          type: 'string',
        },
        relationJPAType: {
          title: 'Type relation',
          type: 'string',
          defaultValue: 'ONETOONE',
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
        fetchType: {
          title: 'Type fetch',
          type: 'string',
          defaultValue: 'DEFAULT',
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
        bidirectionnelle: {
          title: 'Bidirectionnelle',
          type: 'string',
          defaultValue: false,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        findBy: {
          title: 'findBy',
          type: 'string',
          defaultValue: false,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        jsonIgnore: {
          title: 'jsonIgnore',
          type: 'string',
          defaultValue: false,
          editor: {
            type: 'list',
            config: {
              list: [
              {title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
        dtoRelation: {
          title: 'Ajout au DTO',
          type: 'string',
          defaultValue: true,
          editor: {
            type: 'list',
            config: {
              list: [{title: 'Oui', value: true},
              {title: 'Non', value: false},
            ],
            },
          },
        },
      },
    },
    DEFAULT_CASCADE_SETTING : {
      actions: {
        position: 'right',
      },
      cascade: {
        cascadeButtonContent: '<i class="nb-arrow-retweet"</i>',
      },
      add: {
        addButtonContent: '<i class="nb-plus"</i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        cascadeTypes: {
          title: 'Cascade type',
          type: 'string',
          defaultValue: 'ALL',
          editor: {
            type: 'list',
            config: {
              list: [],
            },
          },
        },
      },
    },
};
