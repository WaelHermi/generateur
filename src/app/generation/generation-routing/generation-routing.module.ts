import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WizardComponent } from '../wizard/wizard.component';
import { GenerationComponent } from '../generation.component';

const routes: Routes = [{
  path: '',
  component: GenerationComponent,
  children: [
    {
      path: 'wizard',
      component: WizardComponent,
    },
    {
      path: '',
      redirectTo: 'wizard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: WizardComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenerationRoutingModule { }
