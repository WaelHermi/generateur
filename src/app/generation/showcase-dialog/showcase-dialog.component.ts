import {Component, DoCheck, Input, IterableDiffers, OnInit} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { TABLE_SETTINGS } from '../wizard/table-settings';
import { Entity } from '../models/entity';
import { EnumWrapper } from '../models/enum-wrapper';
import { TypeService } from '../services/type.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MessageService} from "primeng/api";
import {Attribut} from "../models/attribut";
import { Relation } from '../models/relation';
import {even} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-showcase-dialog',
  templateUrl: 'showcase-dialog.component.html',
  styleUrls: ['showcase-dialog.component.scss'],
})
export class ShowcaseDialogComponent implements OnInit {

  @Input() id: Number;
  @Input() advanced: Boolean = false;
  @Input() entity: Entity;
  @Input() entities : Entity[];
  @Input() settings: any;
  @Input() source: any;
  @Input() title: string;
  @Input() types: any;

  inheritanceTypes: EnumWrapper[] = [];
  advancedForm: FormGroup
  cascadeSettings = TABLE_SETTINGS.DEFAULT_CASCADE_SETTING;
  cascadeSource: String[];
  cascadeTable = false;
  nbSpinner: Boolean = false;

  constructor(protected ref: NbDialogRef<ShowcaseDialogComponent>, private typesService: TypeService, private fb: FormBuilder, private messageService: MessageService) { }
  ngOnInit(): void {
    this.nbSpinner = true;
    if(this.advanced) {
      this.advancedForm = this.fb.group({
        discriminatorColumnCtrl: [ this.entity.discriminatorColumn == null? 'DTYPE' : this.entity.discriminatorColumn, Validators.required],
        discriminatorValueCtrl: [ this.entity.discriminatorValue == null? this.entity.nomEntity : this.entity.discriminatorValue,  Validators.required],
        primaryKeyJoinColumnCtrl: [ this.entity.primaryKeyJoinColumn, Validators.required]
      });
      this.typesService.getAllInheritanceTypes().subscribe( response => {
        this.inheritanceTypes = response;
        this.nbSpinner = false;
      }, error => this.nbSpinner = false)
    }else {
      this.nbSpinner = false;
    }
  }

  dismiss() {
    this.ref.close();
  }

  onCascadeOpen(event): void {
    this.nbSpinner = true;
    this.cascadeTable = !this.cascadeTable;
    if(this.source[this.source.indexOf(event.data)].cascadeTypes == null){
      this.source[this.source.indexOf(event.data)].cascadeTypes = [];
      this.cascadeSource = this.source[this.source.indexOf(event.data)].cascadeTypes;
    }else{
      this.cascadeSource = this.source[this.source.indexOf(event.data)].cascadeTypes;
    }
    this.cascadeSettings.columns.cascadeTypes.editor.config.list = this.types.map( element =>
      ({ value: element.nomEnum, title: element.valeurEnum }));
    this.nbSpinner = false;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Etes-vous sûr que vous voulez le supprimer')) {
      event.confirm.resolve();
      const index = this.source.indexOf(event.data);
      this.source.splice(index, 1);
    } else {
      event.confirm.reject();
    }
  }

  quitterAdvanced() {
    if( this.entity.discriminatorColumn == null && this.entity.discriminatorColumn == null && this.entity.primaryKeyJoinColumn == null) {
      this.entity.inheritanceType = 'NON';
    }
  }

  onAdvancedSave() {
    this.nbSpinner = true;
    if(this.entity.inheritanceType == 'NON') {
      this.entity.discriminatorColumn = null;
      this.entities.filter( element => element.nomMere == this.entity.nomEntity).forEach( element => element.discriminatorValue = null && element.primaryKeyJoinColumn == null )
    }
    else if(this.entity.inheritanceType == 'SINGLE_TABLE') {
      if(this.advancedForm.value.discriminatorColumnCtrl != null && this.advancedForm.value.discriminatorColumnCtrl != '') {
        this.entity.discriminatorColumn = this.advancedForm.value.discriminatorColumnCtrl;
      }
      this.entities.filter( element => element.nomMere == this.entity.nomEntity).forEach( element => element.primaryKeyJoinColumn == null);
    }
    else if(this.entity.inheritanceType == 'JOINED') {
      this.entities.filter( element => element.nomMere == this.entity.nomEntity).forEach( element => element.discriminatorValue = null);
    }
    else if (this.entity.inheritanceType == 'TABLE_PER_CLASS') {
      this.entities.filter( element => element.nomMere == this.entity.nomEntity).forEach( element => element.discriminatorValue = null && element.primaryKeyJoinColumn == null )
    }

    if(this.entity.mere && this.entity.mere.inheritanceType == 'JOINED'){
      if(this.advancedForm.value.primaryKeyJoinColumnCtrl != null && this.advancedForm.value.primaryKeyJoinColumnCtrl != '') {
        this.entity.primaryKeyJoinColumn = this.advancedForm.value.primaryKeyJoinColumnCtrl;
      }
    } else if(this.entity.mere && this.entity.mere.inheritanceType == 'SINGLE_TABLE') {
      if(this.advancedForm.value.discriminatorValueCtrl != null && this.advancedForm.value.discriminatorValueCtrl != '') {
        this.entity.discriminatorValue = this.advancedForm.value.discriminatorValueCtrl;
      }
    }
    this.nbSpinner = false;
  }

  onCreateElement(event): void {
    if(this.id == 1 || this.id == 2){
      this.createAttribut(event);
    }else if(this.id == 3){
      this.createRelation(event);
    }
  }

  createAttribut(event){
    if(event.newData.nomAttribut == null || event.newData.nomAttribut == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Le nom de l'attribut est obligatoire",
        life: 3000,
      });
    }
    else if(this.source.filter( element => element.nomAttribut != null && event.newData.nomAttribut != null && element.nomAttribut.toUpperCase() == event.newData.nomAttribut.toUpperCase() ).length > 0){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "L'attribut " +  event.newData.nomAttribut  + " existe déjà ",
        life: 3000,
      });
    }
    else{
      this.source.push(event.newData)
      event.confirm.resolve();
    }
  }

  createRelation(event){

    if(event.newData.nomEntity == null || event.newData.nomEntity == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir une entité",
        life: 3000,
      });
    }
    else if(event.newData.nomRelation == null || event.newData.nomRelation == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir le nom de la relation chez l'entité source",
        life: 3000,
      });
    }
    else if(this.source.filter( element => element.nomRelation != null && event.newData.nomRelation != null && element.nomRelation.toUpperCase() == event.newData.nomRelation.toUpperCase() ).length > 0){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Le nom de la relation " +  event.newData.nomRelation  + " existe déjà ",
        life: 3000,
      });
    }
    else if(event.newData.biNomRelation == null || event.newData.biNomRelation == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir le nom de la relation chez l'entité de destination",
        life: 3000,
      });
    }
    else{
      event.confirm.resolve();
    }
  }

  onEditElement(event): void {
    if(this.id == 1 || this.id == 2){
      this.editAttribut(event);
    }else if(this.id == 3){
      this.editRelation(event);
    }
  }

  editAttribut(event){
    if(event.newData.nomAttribut == null || event.newData.nomAttribut == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Le nom de l'attribut est obligatoire",
        life: 3000,
      });
    }
    else if(this.source.filter( element => event.newData.nomAttribut != null && element.nomAttribut != null && element.nomAttribut.toUpperCase() == event.newData.nomAttribut.toUpperCase()).length > 0 && event.data.nomAttribut != event.newData.nomAttribut){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "L'attribut " +  event.newData.nomAttribut  + " existe déjà ",
        life: 3000,
      });
    }else {
      event.confirm.resolve()
    }
  }


  editRelation(event){

    if(event.newData.nomEntity == null || event.newData.nomEntity == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir une entité",
        life: 3000,
      });
    } else if(event.newData.nomRelation == null || event.newData.nomRelation == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir le nom de la relation chez l'entité source",
        life: 3000,
      });
    } else if(this.source.filter( element => element.nomRelation != null && event.newData.nomRelation != null && element.nomRelation.toUpperCase() == event.newData.nomRelation.toUpperCase()).length > 0 && event.data.nomRelation.toUpperCase() != event.newData.nomRelation.toUpperCase()){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Le nom de la relation " +  event.newData.nomRelation  + " existe déjà ",
        life: 3000,
      });
    } else if(event.newData.biNomRelation == null || event.newData.biNomRelation == ''){
      this.messageService.add({
        severity: "error",
        summary: "Échec",
        detail: "Il faut choisir le nom de la relation chez l'entité de destination",
        life: 3000,
      });
    }
    else{
      event.confirm.resolve();
    }
  }
}
