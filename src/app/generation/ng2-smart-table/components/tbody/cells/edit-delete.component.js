import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Grid } from '../../../lib/grid';
import { Row } from '../../../lib/data-set/row';
import { DataSource } from '../../../lib/data-source/data-source';
var TbodyEditDeleteComponent = /** @class */ (function () {
    function TbodyEditDeleteComponent() {
        this.edit = new EventEmitter();
        this.delete = new EventEmitter();
        this.relationUpdate = new EventEmitter();
        this.attributUpdate = new EventEmitter();
        this.primaryKeyUpdate = new EventEmitter();
        this.advancedUpdate = new EventEmitter();
        this.cascadeUpdate = new EventEmitter();
        this.editRowSelect = new EventEmitter();
    }
    TbodyEditDeleteComponent.prototype.onEdit = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.editRowSelect.emit(this.row);
        if (this.grid.getSetting('mode') === 'external') {
            this.edit.emit({
                data: this.row.getData(),
                source: this.source,
            });
        }
        else {
            this.grid.edit(this.row);
        }
    };
    TbodyEditDeleteComponent.prototype.onDelete = function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.grid.getSetting('mode') === 'external') {
            this.delete.emit({
                data: this.row.getData(),
                source: this.source,
            });
        }
        else {
            this.grid.delete(this.row, this.deleteConfirm);
        }
    };
    TbodyEditDeleteComponent.prototype.onRelationUpdate = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.relationUpdate.emit({
            data: this.row.getData(),
            source: this.source,
        });
    };
    TbodyEditDeleteComponent.prototype.onAttributUpdate = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.attributUpdate.emit({
            data: this.row.getData(),
            source: this.source,
        });
    };
    TbodyEditDeleteComponent.prototype.onPrimaryKeyUpdate = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.primaryKeyUpdate.emit({
            data: this.row.getData(),
            source: this.source,
        });
    };
    TbodyEditDeleteComponent.prototype.onAdvancedUpdate = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.advancedUpdate.emit({
            data: this.row.getData(),
            source: this.source,
        });
    };
    TbodyEditDeleteComponent.prototype.onCascadeUpdate = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.cascadeUpdate.emit({
            data: this.row.getData(),
            source: this.source,
        });
    };
    TbodyEditDeleteComponent.prototype.ngOnChanges = function () {
        this.isActionEdit = this.grid.getSetting('actions.edit');
        this.isActionDelete = this.grid.getSetting('actions.delete');
        this.isActionRelation = this.grid.getSetting('actions.relation');
        this.isActionAttribut = this.grid.getSetting('actions.attribut');
        this.isActionPrimaryKey = this.grid.getSetting('actions.primaryKey');
        this.isActionAdvanced = this.grid.getSetting('actions.advanced');
        this.isActionCascade = this.grid.getSetting('actions.cascade');
        this.editRowButtonContent = this.grid.getSetting('edit.editButtonContent');
        this.deleteRowButtonContent = this.grid.getSetting('delete.deleteButtonContent');
        this.relationRowButtonContent = this.grid.getSetting('relation.relationButtonContent');
        this.attributRowButtonContent = this.grid.getSetting('attribut.attributButtonContent');
        this.primaryKeyRowButtonContent = this.grid.getSetting('primaryKey.primaryKeyButtonContent');
        this.advancedRowButtonContent = this.grid.getSetting('advanced.advancedButtonContent');
        this.cascadeRowButtonContent = this.grid.getSetting('cascade.cascadeButtonContent');

    };
    TbodyEditDeleteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ng2-st-tbody-edit-delete',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template:
                    "\n    <a href=\"#\" nbTooltip=\"La clé primaire\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionPrimaryKey\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n       (click)=\"onPrimaryKeyUpdate($event)\"><nb-icon icon=\"attach-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Les attributs\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionAttribut\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n        (click)=\"onAttributUpdate($event)\"><nb-icon icon=\"list-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Les relations\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionRelation\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n        (click)=\"onRelationUpdate($event)\"><nb-icon icon=\"swap-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Options d'héritage\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionAdvanced\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n        (click)=\"onAdvancedUpdate($event)\"><nb-icon icon=\"options-2-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Cascade\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionCascade\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n        (click)=\"onCascadeUpdate($event)\"><nb-icon icon=\"share-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Modification\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionEdit\" class=\"ng2-smart-action ng2-smart-action-edit-edit\"\n        (click)=\"onEdit($event)\"><nb-icon icon=\"edit-2-outline\" pack=\"eva\"></nb-icon></a>\n    " +
                    "<a href=\"#\" nbTooltip=\"Suppression\" nbTooltipPlacement=\"bottom\" nbTooltipStatus=\"info\" status=\"info\" *ngIf=\"isActionDelete\" class=\"ng2-smart-action ng2-smart-action-delete-delete\"\n        (click)=\"onDelete($event)\"><nb-icon icon=\"trash-2-outline\" pack=\"eva\"></nb-icon></a>\n    " ,

                },] },
    ];
    /** @nocollapse */
    TbodyEditDeleteComponent.propDecorators = {
        "grid": [{ type: Input },],
        "row": [{ type: Input },],
        "source": [{ type: Input },],
        "deleteConfirm": [{ type: Input },],
        "editConfirm": [{ type: Input },],
        "edit": [{ type: Output },],
        "delete": [{ type: Output },],
        "relationUpdate": [{ type: Output },],
        "attributUpdate": [{ type: Output },],
        "primaryKeyUpdate": [{ type: Output },],
        "advancedUpdate": [{ type: Output },],
        "cascadeUpdate": [{ type: Output },],
        "editRowSelect": [{ type: Output },],
    };
    return TbodyEditDeleteComponent;
}());
export { TbodyEditDeleteComponent };
//# sourceMappingURL=edit-delete.component.js.map
