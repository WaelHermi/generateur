import { environment } from '../../environments/environment';

const BASE = environment.API_IP;
const PORT = environment.API_PORT;
const VERSION = '/v1';

export const API_MAIN_URLS = {
    UTILISATEUR_URL: BASE + PORT + VERSION + '/utilisateur',
    ROLE_URL: BASE + PORT + VERSION + '/role',
    GENERATED_PROJECT_URL: BASE + PORT + VERSION + '/generatedProject',
    REQUEST_RESET_PASSWORD_URL: BASE + PORT + VERSION + '/SendEmailToResetPassword',
    RESET_PASSWORD_URL: BASE + PORT + VERSION + '/utilisateur/resetPassword',
    UPDATE_PASSWORD_URL: BASE + PORT + VERSION + '/utilisateur/updatePassword'

};

