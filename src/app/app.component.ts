/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit, KeyValueDiffer, KeyValueDiffers } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { SeoService } from './@core/utils/seo.service';
import { AuthService } from './authentification/auth.service';
import { Fonctionnalite } from './models/fonctionnalite';
import { Router } from '@angular/router';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-app',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AppComponent implements OnInit {

  differ: KeyValueDiffer<string, any>;
  constructor(private analytics: AnalyticsService, private seoService: SeoService,
    private authService: AuthService, private router: Router, private differs: KeyValueDiffers,
    ) {
      this.differ = this.differs.find({}).create();
  }

  menu: NbMenuItem[] = [];

  ngDoCheck() {
    const change = this.differ.diff(this.authService.currentUserValue);
    if (change) {
      if (this.authService.currentUserValue != null) {

        const fonctionnalites: Fonctionnalite [] = [];
        this.authService.currentUserValue.utilisateur.roles
        .forEach(element =>  element.fonctionnalites
          .forEach(elementFonc => {
          fonctionnalites.push(elementFonc);
        }));
        this.menu = [];
        fonctionnalites.map(element => this.menu.push({
          title: element.designationFonctionnalite as string,
          icon: element.iconFonctionnalite as string,
          link: element.urlFonctionnalite as string,
        }));
      }
    }
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.seoService.trackCanonicalChanges();
    if (this.authService.currentUserValue != null) {

      const fonctionnalites: Fonctionnalite [] = [];
      this.authService.currentUserValue.utilisateur.roles.forEach(element =>  element.fonctionnalites.forEach(elementFonc => {
        fonctionnalites.push(elementFonc);
      }));
      fonctionnalites.map(element => this.menu.push({
        title: element.designationFonctionnalite as string,
        icon: element.iconFonctionnalite as string,
        link: element.urlFonctionnalite as string,
      }));      
    }
  }  
}