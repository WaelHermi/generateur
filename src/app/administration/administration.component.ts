import { Component, OnInit } from '@angular/core';
import { Fonctionnalite } from '../models/fonctionnalite';
import { AuthService } from '../authentification/auth.service';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-administration',
  template:`
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
    `,
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {

  menu: NbMenuItem[] = [];

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {

    if (this.authService.currentUserValue != null) {

      const fonctionnalites: Fonctionnalite [] = [];
      this.authService.currentUserValue.utilisateur.roles.forEach(element =>  element.fonctionnalites.forEach(elementFonc => {
        fonctionnalites.push(elementFonc);
      }));
      fonctionnalites.map(element => this.menu.push({
        title: element.designationFonctionnalite as string,
        icon: element.iconFonctionnalite as string,
        link: element.urlFonctionnalite as string,
      }));      
    }
  }  

}
