import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrationComponent } from './administration.component'
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { TraceAccessComponent } from './trace-access/trace-access.component';
import { TraceDonneesComponent } from './trace-donnees/trace-donnees.component';
import { ProfilComponent } from './profil/profil.component';

const routes: Routes = [{
  path: '',
  component: AdministrationComponent,
  children: [
    {
      path: 'utilisateur',
      component: UtilisateurComponent,
    },
    {
      path: 'trace_acces',
      component: TraceAccessComponent,
    },
    {
      path: 'trace_donnees',
      component: TraceDonneesComponent,
    },
    {
      path: 'profil',
      component: ProfilComponent,
    },
    {
      path: '',
      redirectTo: 'utilisateur',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: UtilisateurComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationRoutingModule { }
