import { TestBed } from '@angular/core/testing';

import { TraceDonneesService } from './trace-donnees.service';

describe('TraceDonneesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TraceDonneesService = TestBed.get(TraceDonneesService);
    expect(service).toBeTruthy();
  });
});
