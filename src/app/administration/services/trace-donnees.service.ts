import { Injectable } from '@angular/core';
import { CrudService } from '../../services/curd.service';
import { TraceDonnees } from '../models/trace-donnees';
import { API_ADMINISTRATION_URLS } from '../config/api.url.config';

@Injectable({
  providedIn: 'root'
})
export class TraceDonneesService extends CrudService<TraceDonnees>{
  url = API_ADMINISTRATION_URLS.TRACE_DONNEES_URL;
}
