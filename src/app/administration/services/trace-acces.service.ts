import { Injectable } from '@angular/core';
import { CrudService } from '../../services/curd.service';
import { TraceAcces } from '../models/trace-acces';
import { API_ADMINISTRATION_URLS } from '../config/api.url.config';

@Injectable({
  providedIn: 'root'
})
export class TraceAccesService extends CrudService<TraceAcces> {
  url = API_ADMINISTRATION_URLS.TRACE_ACCES_URL;
}
