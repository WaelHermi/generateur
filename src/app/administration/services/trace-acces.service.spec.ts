import { TestBed } from '@angular/core/testing';

import { TraceAccesService } from './trace-acces.service';

describe('TraceAccesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TraceAccesService = TestBed.get(TraceAccesService);
    expect(service).toBeTruthy();
  });
});
