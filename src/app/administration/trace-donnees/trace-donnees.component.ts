import { Component, OnInit } from '@angular/core';
import { TraceDonnees } from '../models/trace-donnees';
import { DataModel } from '../shared/data-model';
import { TraceDonneesService } from '../services/trace-donnees.service';
import { SelectModel } from '../shared/select-model';

@Component({
  selector: 'ngx-trace-donnees',
  templateUrl: './trace-donnees.component.html',
  styleUrls: ['./trace-donnees.component.scss']
})
export class TraceDonneesComponent implements OnInit {

  traceDonneess: TraceDonnees[] = [];

  traceDonneesModel: DataModel[];

  constructor(private traceDonneesService: TraceDonneesService) { }

  ngOnInit(){

    this.traceDonneesService.getAll(10, 0).subscribe( data => {
      this.traceDonneess = data;
    })

    this.traceDonneesModel = [
      new DataModel('idTraceDonnes','ID','number', false, false, false, [], false),
      new DataModel('utilisateur','Utilisateur','select', true, false, false, [], false, new SelectModel('idUtilisateur', 'usernameUtilisateur')),
      new DataModel('operation','Opération','text', true, false, false,[], false),
      new DataModel('date','Date','date', true, false, false, [],  true),
      new DataModel('navigateur','Navigateur','text', true, false, false,[], false),
      new DataModel('versionNavigateur','Version','text', true, false, false,[], false),
      new DataModel('ip','Adresse IP','text', true, false, false,[], false),

    ]
  }

}
