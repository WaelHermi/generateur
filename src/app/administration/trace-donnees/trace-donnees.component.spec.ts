import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraceDonneesComponent } from './trace-donnees.component';

describe('TraceDonneesComponent', () => {
  let component: TraceDonneesComponent;
  let fixture: ComponentFixture<TraceDonneesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraceDonneesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraceDonneesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
