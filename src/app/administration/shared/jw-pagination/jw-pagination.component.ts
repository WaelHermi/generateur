import { Component, OnInit, Input, Output, SimpleChanges, OnChanges, EventEmitter } from '@angular/core';
import {CrudService} from '../../../services/curd.service';


@Component({
  selector: 'ngx-jw-pagination',
  templateUrl: './jw-pagination.component.html',
  styleUrls: ['./jw-pagination.component.scss'],
})
export class JwPaginationComponent implements OnInit , OnChanges {

  @Input() items: number;
  @Output() changePage = new EventEmitter<any>(true);
  @Input() initialPage = 1;
  @Input() pageSize = 10;
  @Input() maxPages = 3;
  @Input() crudService: CrudService<any>;

  pager: any = {};
  constructor() { }

  ngOnInit() {
    /*this.crudService.count().subscribe( reponse => this.items = reponse.result,
      );*/
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setPage(this.initialPage);
  }

  private setPage(page: number) {
   /* this.pager.startIndex = 1;*/
    const paginate = require('jw-paginate');
    // get new pager object for specified page
    this.pager = paginate(this.items, page, this.pageSize, this.maxPages);
    let data: any[] = [];
    let pageOfItems = [];
    const pageIndex = this.pager.currentPage - 1;
    console.log(page);
    this.crudService.getAll(10, pageIndex).subscribe( response => { data = response
      data != null && data.length ? pageOfItems = data : [];
      // call change page function in parent component
      this.changePage.emit(pageOfItems);
    });

     /* data != null && data.length ? pageOfItems = data : [];
      // call change page function in parent component
      this.changePage.emit(pageOfItems);*/
   }
}
