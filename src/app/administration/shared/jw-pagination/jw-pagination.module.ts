import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JwPaginationComponent } from './jw-pagination.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [JwPaginationComponent],
  exports: [
    JwPaginationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
})
export class JwPaginationModule { }
