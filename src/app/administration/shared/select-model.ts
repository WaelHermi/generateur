export class SelectModel {
    constructor(
        public selectId: string,
        public selectNom: string,
        public selectModelList?: any[]) {}
}
