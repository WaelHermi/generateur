import {Component, Input, OnInit} from '@angular/core';
import {FormGroup,} from '@angular/forms';
import {CrudService} from '../../../services/curd.service';
import {DataModel} from '../../shared/data-model';
import {GridApi} from 'ag-grid-community';
import {SortableHeaderComponent} from '../../../ag-grid/sortable-header/sortable-header.component';
import {ButtonRendererComponent} from '../../button-renderer/button-renderer.component';
import {formatDate} from '@angular/common';
import {ReactiveFormConfig} from "@rxweb/reactive-form-validators";

@Component({
  selector: 'ngx-curd',
  templateUrl: './curd.component.html',
  styleUrls: ['./curd.component.scss']
})
export class CurdComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  entity: String;

  @Input()
  service: CrudService<any>;

  @Input()
  initItem: any;

  @Input()
  idName: string = '';

  @Input()
  operationForm: FormGroup;

  @Input()
  dataModelList: DataModel[] = [];

  @Input()
  create: Boolean = true;

  @Input()
  actions: Boolean = true;

  @Input()
  operationTable;

  @Input()
  updateEntity;

  crudType = 'sample';
  enableRtl='false';

  columnDefsOperation = [];
  isOperationSubmitted: Boolean = false;
  displayOperationUpdate: Boolean = false;
  displayOperationDelete: Boolean = false;
  displayOperation: Boolean = false;
  nbSpinnerOperation: boolean;
  blockButton: boolean = false;

  gridApiOperation: GridApi;
  gridColumnApiOperation: any;
  defaultColDef: { filter: boolean; resizable: boolean; sortable: boolean; headerComponent: string; headerComponentParams: { menuIcon: string; template: string; }; };
  frameworkComponents: { sortableHeaderComponent: any; buttonRenderer: any; };
  domLayout: string;
  localeText = {};
  private items: number;

  booleanOptions = [ { label: 'Oui', value: true }, { label: 'Non', value: false } ];
  private idValue: any;
  event: any;

  formControlsOperations() { return this.operationForm.controls; }

  constructor(){

  }

  ngOnInit(){

    ReactiveFormConfig.set({

      "validationMessage":{

        "required":"Ce champ est requis!",

        "minLength":"La longueur minimale est de {{1}}!",

        "maxLength":"la longueur maximale autorisée est de {{1}}!",

      }

    });

    this.loaddataOperation()

    this.localeText = {
      page: 'page',
      more: 'plus',
      of: 'de',
      to: 'à',
      next: 'suivant',
      last: 'dernier',
      first: 'premier',
      previous: 'précédent',
      loadingOoo: 'chargement',
      selectAll: 'tout sélectionner',
      searchOoo: 'chercher',
      blanks: 'blancs',
      filterOoo:  'filtre',
      applyFilter: 'appliquer filtre',
      equals:  'équivaut à',
      notEqual: 'inégal',
      lessThan:  'moins que',
      greaterThan: 'plus grand que',
      lessThanOrEqual:  'inférieur ou égal',
      greaterThanOrEqual:  'supérieur ou égale',
      inRange:  'dans la gamme',
      contains:  'contient',
      notContains: 'ne contient pas',
      startsWith:  'commence avec"',
      endsWith: 'se termine par',
      andCondition:  'et condition',
      orCondition:  'ou condition',
      group:  'groupe',
      columns:  'colonnes',
      filters:  'filtres',
      noRowsToShow:  'aucune ligne à afficher'

    };

    this.defaultColDef = { filter: true ,resizable: true,sortable: true,
      headerComponent: 'sortableHeaderComponent',
      headerComponentParams: {
        menuIcon: 'fa-bars',
        template:
          '<div class="ag-cell-label-container" role="presentation">' +
          '  <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
          '  <div ref="eLabel" class="ag-header-cell-label" role="presentation">' +
          '    <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>' +
          '    <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>' +
          '    <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>' +
          '    <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>' +
          '    ** <span ref="eText" class="ag-header-cell-text" role="columnheader"></span>' +
          '    <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>' +
          '  </div>' +
          '</div>'
      }
    };
    this.domLayout = 'autoHeight';
    this.frameworkComponents = {
      sortableHeaderComponent: SortableHeaderComponent,
      buttonRenderer: ButtonRendererComponent,
    };

    this.service.count().subscribe( reponse => this.items = reponse.result );
  }

  dataChanged($event) {
    this.operationTable = this.operationTable.concat($event);
  }

  loaddataOperation() {
      this.dataModelList.forEach( element => {
        if (element.type == 'select' && element.read) {
          this.columnDefsOperation.push({
            headerName: element.columnReference,
            valueGetter: function(params) {
              return params.data[element.columnName][element.selectModel.selectNom];
            },
          })
        } else if(element.columnReference == 'Status' && element.read){
          this.columnDefsOperation.push({
            headerName: element.columnReference,
            valueGetter: function(params) {
              if(params.data[element.columnName] == true){
                return 'Succés';
              }else {
                return 'Échec';
              };
            },
          })
        }
        else if(element.columnReference == 'État' && element.read){
          this.columnDefsOperation.push({
            headerName: element.columnReference,
            valueGetter: function(params) {
              if(params.data[element.columnName] == true){
                return 'Activé';
              }else {
                return 'Bloqué';
              };
            },
          })
        }
        else if(element.type == 'date' && element.read){
            this.columnDefsOperation.push({
              headerName: element.columnReference,
              valueGetter: function(params) {
                if(params.data[element.columnName] != null){
                  const format = 'dd/MM/yyyy - HH:mm';
                  const locale = 'en-US';
                  const formattedDate = formatDate(params.data[element.columnName], format, locale);
                  return formattedDate;
                }else{
                  return '';
                }
              }
            })
        }else if(element.type == 'multiselect' && element.read ){
          this.columnDefsOperation.push({
            headerName: element.columnReference,
            valueGetter: function(params) {
              if(params.data[element.columnName] != null && Array.isArray(params.data[element.columnName])){
                let rolesString = '';
                Array.prototype.forEach.call(params.data[element.columnName], child => {
                  let roleName = child.nomRole.split("_").pop();
                  rolesString = rolesString + roleName.substring(0, 1).toUpperCase() + roleName.substring(1).toLowerCase() + ', ';
                });
                return rolesString;
              }else{
                return '';
              }
            }
          })
        }
        else if(element.read) {
          this.columnDefsOperation.push({ headerName: element.columnReference, field: element.columnName })
        }
      });
      if(this.actions){
        this.columnDefsOperation.push({
          headerName:"Action",
          cellRenderer: 'buttonRenderer',
          cellRendererParams: {
            onClick: this.onBtnClickOperation.bind(this),
            label: 'Action',
          },
        });
      }
    }

  createOperation() {

    this.isOperationSubmitted = true;
    this.nbSpinnerOperation = true;
    this.blockButton = true;
    console.log(this.operationForm.value);
    console.log(this.operationForm.invalid);
    if(!this.operationForm.invalid){
      this.initItem = this.operationForm.value;
      this.service.add(this.initItem).subscribe( response => {
        this.gridApiOperation.updateRowData({ add: [response] });
        this.operationForm.reset();
        this.displayOperation = false
        this.isOperationSubmitted = false;
        this.nbSpinnerOperation = false;
        this.blockButton = false;
      });
    }

    this.nbSpinnerOperation = false;
    this.blockButton = false;

    console.log(this.operationForm.value)

  }

  updateOperation(){
    this.isOperationSubmitted = true;
    this.nbSpinnerOperation = true;
    this.blockButton = true;
    this.initItem = this.operationForm.value;
    this.initItem[this.idName] = this.idValue;
    this.service.update(this.initItem).subscribe( response => {
      this.gridApiOperation.updateRowData({ remove: [this.event] });
      this.gridApiOperation.updateRowData({ add: [response] });
      this.operationForm.reset();
      this.displayOperationUpdate = false
      this.isOperationSubmitted = false;
      this.nbSpinnerOperation = false;
      this.blockButton = false;
    });
  }

  deleteOperation(){
    this.nbSpinnerOperation=true;
    this.blockButton=true;
    this.service.delete(this.initItem[this.idName]).subscribe( response => {
      if(response.result){
        this.gridApiOperation.updateRowData({ remove: [this.initItem] });
        this.displayOperationDelete=false;
      }
      this.nbSpinnerOperation=true;
      this.blockButton=true;
    }
    )
  }

  onBtnClickOperation(e){
    if(e.id==0){
      this.initItem=e.rowData;
      this.displayOperationDelete=true;
    }
    else if(e.id==1) {
      this.idValue = e.rowData[this.idName];
      this.event = e.rowData;
      this.displayOperationUpdate=true;
      this.operationForm.patchValue(this.event);
      this.initItem=e.rowData;
      this.dataModelList.forEach( element => {
        this.operationForm.patchValue(e.rowData);
      })
      this.displayOperationUpdate=true;
    }
  }

  onGridReadyOperation(params) {
    this.gridApiOperation = params.api;
    this.gridColumnApiOperation = params.columnApi;
    params.api.expandAll();
    params.api.sizeColumnsToFit();
    this.gridApiOperation.paginationSetPageSize(10);
    params.api.sizeColumnsToFit();
    window.addEventListener("resize", function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.operationTable = pageOfItems;
  }
}
