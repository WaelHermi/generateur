import {Component, OnInit} from '@angular/core';
import {DataModel} from '../shared/data-model';
import {Utilisateur} from '../../models/utilisateur';
import {Role} from '../../models/role';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {UtilisateurService} from '../../services/utilisateur.service';
import {ReactiveFormConfig, RxFormBuilder, RxwebValidators} from "@rxweb/reactive-form-validators";
import {PATTERN_CONFIG} from "@rxweb/reactive-form-validators/const/config-names.const";
import {formatDate} from "@angular/common";

@Component({
  selector: 'ngx-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.scss']
})
export class UtilisateurComponent implements OnInit {

  utilisateurs: Utilisateur[] = [];

  roles: Role[] = [];

  utilisateurForm: FormGroup;

  utilisateur: Utilisateur = new Utilisateur();

  utilisateursModel: DataModel[];
  rowData: { nomUtilisateur: string; prenomUtilisateur: string; adresseUtilisateur: string; }[];

  constructor(private utilisateurService: UtilisateurService, private fb: RxFormBuilder, private route: ActivatedRoute) { }

  ngOnInit(){

    this.utilisateurService.getAll(10, 0).subscribe( data => {
      console.log(data)
      this.utilisateurs = data;
    })

    this.utilisateurForm = this.fb.group({
      adresseMailUtilisateur: ['', [RxwebValidators.required(), RxwebValidators.email({message: "Veuillez saisir une adresse mail correcte"})]],
      adresseUtilisateur: [''],
      telephoneUtilisateur: ['', [RxwebValidators.required()]],
      passwordUtilisateur: ['', [RxwebValidators.required(), RxwebValidators.minLength({value: 5}), RxwebValidators.maxLength({value: 25})]],
      usernameUtilisateur: ['', [RxwebValidators.required(), RxwebValidators.minLength({value: 5}), RxwebValidators.maxLength({value: 25})]],
      prenomUtilisateur: ['', [RxwebValidators.required(), RxwebValidators.minLength({value: 5}), RxwebValidators.maxLength({value: 25})]],
      nomUtilisateur: ['', [RxwebValidators.required()]],
      active: [''],
      dateDeNaissanceUtilisateur: [ new Date()],
    });

    this.utilisateursModel = [
      new DataModel('idUtilisateur','ID','number', false, false,  false,[], false),
      new DataModel('nomUtilisateur','Nom','text', true, true, false,  [], true),
      new DataModel('prenomUtilisateur','Prénom','text', true, true, false,[], true),
      new DataModel('usernameUtilisateur','Pseudo','text', true, true, false, [], true),
      new DataModel('passwordUtilisateur','Mot de passe','password', false, true, false, [], true),
      new DataModel('adresseMailUtilisateur','e-mail','email', true, true, false, [], true),
      new DataModel('adresseUtilisateur','Adresse','text', true, true, false, [], false),
      new DataModel('telephoneUtilisateur','Téléphone','number', true, true, false, [], true),
      new DataModel('dateDeNaissanceUtilisateur','Date de naissance','date', false, true, false, [], false),
      new DataModel('roles','Roles','multiselect', true, false, false, [], false),
      new DataModel('active','État','boolean', true, false, true, [], false),
    ]
  }

}
