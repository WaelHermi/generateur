import { Component } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'ngx-button-renderer',
  templateUrl: './button-renderer.component.html',
  styleUrls: ['./button-renderer.component.scss']
})
export class ButtonRendererComponent implements ICellRendererAngularComp {

  params;

  constructor() { }
  accesrole:boolean=false
  
  onClick($event) {
    if (this.params.onClick instanceof Function) {


      const params = {
        event: $event,
        rowData: this.params.node.data,
        id:0
      };

      this.params.onClick(params);

    }
  }
  onClick1($event) {
    if (this.params.onClick instanceof Function) {


      const params = {
        event: $event,
        rowData: this.params.node.data,
        id:1
      };

      this.params.onClick(params);

    }
  }


  agInit(params): void {
    this.params = params;
  }

  refresh(params?: any): boolean {
    return true;
  }

}
