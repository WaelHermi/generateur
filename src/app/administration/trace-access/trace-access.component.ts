import { Component, OnInit } from '@angular/core';
import { TraceAcces } from '../models/trace-acces';
import { DataModel } from '../shared/data-model';
import { TraceAccesService } from '../services/trace-acces.service';

@Component({
  selector: 'ngx-trace-access',
  templateUrl: './trace-access.component.html',
  styleUrls: ['./trace-access.component.scss']
})
export class TraceAccessComponent implements OnInit {

  traceAccess: TraceAcces[] = [];

  traceAccesModel: DataModel[];

  constructor(private traceAccesService: TraceAccesService) { }

  ngOnInit(){


    this.traceAccesService.getAll(10, 0).subscribe( data => {
      this.traceAccess = data;
    })


    this.traceAccesModel = [
      new DataModel('idTraceAcces','ID','number', false, false, false,[], false),
      new DataModel('identifiant','Pseudo','text', true, false, false,[], false),
      new DataModel('date','Date','date', true, false, false,  [], true),
      new DataModel('navigateur','Navigateur','text', true, false, false, [], false),
      new DataModel('versionNavigateur','Version','text', true, false, false, [], false),
      new DataModel('ip','Adresse IP','text', true, false, false, [], false),
      new DataModel('status','Status','text', true, false, false, [], false),
    ]
  }
}
