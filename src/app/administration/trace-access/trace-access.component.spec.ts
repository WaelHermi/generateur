import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraceAccessComponent } from './trace-access.component';

describe('TraceAccessComponent', () => {
  let component: TraceAccessComponent;
  let fixture: ComponentFixture<TraceAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraceAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraceAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
