export class TraceAcces {
    idTraceAcces: String;
    date: Date;
    versionNavigateur: String;
    navigateur: String;
    ip: String;
    status: Boolean;
    identifiant: String;
}
