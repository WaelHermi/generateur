import { Utilisateur } from '../../models/utilisateur'

export class TraceDonnees {

    idTraceDonnees; String;
    date: Date;
    operation: String;
    versionNavigateur: String;
    navigateur: String;
    ip: String;
    utilisateur: Utilisateur;
}
