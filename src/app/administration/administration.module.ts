import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AdministrationComponent } from './administration.component';
import { CurdComponent } from './shared/curd/curd.component';
import { TraceAccessComponent } from './trace-access/trace-access.component';
import { TraceDonneesComponent } from './trace-donnees/trace-donnees.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { AdministrationRoutingModule } from './administration-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import { ButtonRendererComponent } from './button-renderer/button-renderer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {DialogModule} from 'primeng/dialog';
import { ThemeModule } from '../@theme/theme.module';
import { JwPaginationModule } from './shared/jw-pagination/jw-pagination.module';
import {DropdownModule} from 'primeng/dropdown';

import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbListModule,
  NbRouteTabsetModule,
  NbTabsetModule,
  NbActionsModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbMenuModule,
  NbWindowModule,
  NbTooltipModule,
  NbSpinnerModule,
} from '@nebular/theme';
import { ProfilComponent } from './profil/profil.component';
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {RxReactiveFormsModule} from "@rxweb/reactive-form-validators";


@NgModule({
  declarations: [AdministrationComponent, CurdComponent, TraceAccessComponent,
    TraceDonneesComponent, UtilisateurComponent, ButtonRendererComponent, ProfilComponent ],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    AgGridModule.withComponents([
      ButtonRendererComponent,
    ]),
    NbTooltipModule,
    NbSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    DialogModule,
    NbSelectModule,
    NbIconModule,
    NbInputModule,
    NbMenuModule,
    NbWindowModule,
    NbAccordionModule,
    NbRouteTabsetModule,
    NbTabsetModule,
    NbActionsModule,
    ThemeModule,
    JwPaginationModule,
    DropdownModule,
    ToastModule,
    RxReactiveFormsModule
  ],
  providers: [DatePipe, MessageService],
})
export class AdministrationModule { }
