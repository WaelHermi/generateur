import { environment } from '../../../environments/environment';

const BASE = environment.API_IP;
const PORT = environment.API_PORT;
const VERSION = '/v1';


export const API_ADMINISTRATION_URLS = {
    TRACE_ACCES_URL : BASE + PORT + VERSION + '/traceAcces',
    TRACE_DONNEES_URL : BASE + PORT + VERSION + '/traceDonnees',
};
