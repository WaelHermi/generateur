import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../models/utilisateur';
import { ClientService } from '../../authentification/services/client.service';
import { Router } from '@angular/router';
import { UtilisateurService } from '../../services/utilisateur.service';
import {MessageService} from "primeng/api";
import {PasswordModel} from "../../models/password-model";

@Component({
  selector: 'ngx-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  utilisateur: Utilisateur;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  submitted: boolean;
  errors: string[];
  messages: string[];
  getConfigValue(key: string): any {}
  forms: any;
  formspassword: any;
  password:string;
  oldpassword:string;
  confirmepassword:string;
  matchpassword:boolean=false;
  constructor(private utilidateurService: UtilisateurService, private router: Router,private _UtilisateurService: UtilisateurService, private messageService: MessageService) { }

  ngOnInit() {
    this.utilisateur = new Utilisateur();
    this.forms = { validation:
      { nomUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        prenomUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        adresseMailUtilisateur:
        { required: true, minLength: 4, maxLength: 100 },
        adresseUtilisateur:
        { required: false, minLength: 4, maxLength: 200 },
        telephoneUtilisateur:
        { required: true, minLength: 4, maxLength: 20 },
        usernameUtilisateur:
        { required: true, minLength: 4, maxLength: 50 },
      }};
      this.getUserbyid(JSON.parse(localStorage.getItem("currentUser")).utilisateur.idUtilisateur);

      this.formspassword = { validation:
        {
          oldpasswordUtilisateur:
          { required: true, minLength: 4, maxLength: 20  },
          passwordUtilisateur:
          { required: true, minLength: 4, maxLength: 20  },
          confirmepasswordUtilisateur:
          { required: true, minLength: 4, maxLength: 20  },
        }};

    }


  updateMyProfil(): void {
    console.log(this.utilisateur);
    this.utilidateurService.update(this.utilisateur).subscribe( response => {
      this.messageService.add({
        severity: "success",
        summary: "Succès",
        detail: "Votre compte a été modifié avec succès",
        life: 3000,
      });
      this.getUserbyid(JSON.parse(localStorage.getItem("currentUser")).utilisateur.idUtilisateur);
      localStorage.setItem("currentUser", JSON.stringify(response))
    }, error => {
      this.messageService.add({
        severity: "error",
        summary: "Erreur",
        detail: "Un problème est survenu",
        life: 3000,
      });
    });
  }

  resetpassword(): void {
    if(this.password!=this.confirmepassword){
      this.matchpassword=true;
      return;
    }else{
      this.matchpassword=false;
      let passwordModel: PasswordModel = new PasswordModel();
      passwordModel.ancienMotDePasse = this.oldpassword;
      passwordModel.nouveauMotDePasse = this.password;
      this.utilidateurService.updatePassword(passwordModel).subscribe( response => {
        if(response.result) {
          this.messageService.add({
            severity: "success",
            summary: "Succès",
            detail: "Votre mot de passe a été modifié avec succès",
            life: 3000,
          });
        }else {
          this.messageService.add({
            severity: "error",
            summary: "Erreur",
            detail: "Un problème est survenu",
            life: 3000,
          });
        }
      }, error => {
        console.log(error)
        if(error.status == 403){
          this.messageService.add({
            severity: "error",
            summary: "Erreur",
            detail: "Mot de passe incorrect",
            life: 3000,
          });
        }else {
          this.messageService.add({
            severity: "error",
            summary: "Erreur",
            detail: "Un problème est survenu",
            life: 3000,
          });
        }
      });
    }
  //   console.log(this.utilisateur);
  //   this.clientSerivce.add(this.utilisateur).subscribe( () => {
  //     this.router.navigate(['/auth/login']);
  // });
  }

  getUserbyid(id:string){
    this._UtilisateurService.getById(id).subscribe(res=>{
      this.utilisateur=res
    })
  }

}
